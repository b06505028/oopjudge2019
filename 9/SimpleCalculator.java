import java.text.DecimalFormat;

public class SimpleCalculator {
	private double value;
	private String operator;
	private int count = 0;
	private double result;
	private boolean endFlag, unknownOperator, unknownValue;
	//flags to help me distinguish what error is it
	private DecimalFormat formatter = new DecimalFormat("###0.00");

	public void calResult(String cmd) throws UnknownCmdException {
		unknownOperator = false;
		unknownValue = false;
		String[] command = cmd.split(" ");
		//separate content of cmd by space
		if (command.length == 1) //if input only one word
			if (endCalc(command[0])) {
				endFlag = true;System.out.println("this");}
			else
				throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		else if (command.length == 2) { 
			try {
				value = Double.valueOf(command[1]);
			} catch (Exception e) { //catch NumberFormatException
				unknownValue = true;
			}
			operator = command[0];
			switch (operator) {
			case "+":
				if(!unknownValue) {// if unknownValue, do nothing
				result += value;
				count++;
				}
				break;
			case "-":
				if(!unknownValue){// if unknownValue, do nothing
				result -= value;
				count++;
				}
				break;
				
			case "*":
				if(!unknownValue){// if unknownValue, do nothing
				result *= value;
				count++;
				}
				break;
				
			case "/":
				if(!unknownValue){// if unknownValue, do nothing
				if (value == 0)
					throw new UnknownCmdException("Can not divide by 0");
				result /= value;
				count++;
				}
				break;
				
			default:
				unknownOperator = true;//if operator isn't '+','-','*','/', it's unknownOperator
			}
			if (unknownValue && unknownOperator)
				throw new UnknownCmdException(
						command[0] + " is an unknown operator and " + command[1] + " is an unknown value");
			else if (unknownValue)
				throw new UnknownCmdException(command[1] + " is an unknown value");
			else if (unknownOperator)
				throw new UnknownCmdException(command[0] + " is an unknown operator");
		} else //if length of cmd is over 2
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
	};

	public String getMsg() {
		if (endFlag)
			return "Final result = " + formatter.format(result);
		switch (count) {
		case 0:
			return "Calculator is on. Result = " + formatter.format(result);
		case 1:
			return new StringBuffer("Result ").append(operator).append(" ").append(formatter.format(value))
					.append(" = ").append(formatter.format(result)).append(". New result = ")
					.append(formatter.format(result)).toString();
		//use StringBuffer to increase efficiency
		default:
			return new StringBuffer("Result ").append(operator).append(" ").append(formatter.format(value))
					.append(" = ").append(formatter.format(result)).append(". Updated result = ")
					.append(formatter.format(result)).toString();
		}
	};

	public boolean endCalc(String cmd) {
		if("r".equalsIgnoreCase(cmd)) { //if cmd equals r
			endFlag=true;
			return true;
			
		}
		return false;
	};
}