
public class GreenCrud {
	public int calPopulation(int initialSize, int days) {
		int periods=days/5,earlierSum=0,sum=initialSize,temp=0;
		for(int i=0;i<periods;i++) {
			temp=sum; //store the number , it will be used in next circle
			sum=sum+earlierSum; //add the number at present and the number last time
			earlierSum=temp; //pass original "sum", taken as the "earlierSum" in next circle
		}
		/*
		 * code in for means the action crud's number do
		 * when it have been done "peroids" times, jump out
		*/
		return sum;
	}
}
