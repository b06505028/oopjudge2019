
public class Account {
	private int balance;
	public Account(int balance) {
		setBalance(balance);
	} // constructor can set balance
	public int getBalance() {
		return balance;
	} //balance getter
	public void setBalance(int balance) {
		this.balance = balance;
	} //balance setter
}
