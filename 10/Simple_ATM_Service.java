
public class Simple_ATM_Service implements ATM_Service {
	
	public boolean checkBalance(Account account, int money) throws ATM_Exception{
		if(account.getBalance()>money) //if balance is enoutgh
		return true;
		else throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
	}
	
	public boolean isValidAmount(int money) throws ATM_Exception{
		if(money%1000 ==0) //if it can divided by 1000
		return true;
		else throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
	};
	
	public void withdraw(Account account, int money) {
		try {
		if(checkBalance(account, money)&&isValidAmount(money)) //only when they both true, it can run without catching and go next sentence
			account.setBalance(account.getBalance()-money);
		}
		catch (ATM_Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("updated balance : "+account.getBalance());
	};
}
