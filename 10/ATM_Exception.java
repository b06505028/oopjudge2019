
public class ATM_Exception extends Exception {
	
	public static enum ExceptionTYPE{
		BALANCE_NOT_ENOUGH,AMOUNT_INVALID
	} //declare enum as a inner class of ATM_Exception
	
	private ExceptionTYPE exceptionCondition;
	
	public String getMessage() {
		if(exceptionCondition==ExceptionTYPE.BALANCE_NOT_ENOUGH)
			return "BALANCE_NOT_ENOUGH";
		else return "AMOUNT_INVALID";
	} //override getMessage from super class 
	
	ATM_Exception(ExceptionTYPE a ){
		exceptionCondition=a;
		} //constructor
}
	
