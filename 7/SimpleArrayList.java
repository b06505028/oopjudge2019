
public class SimpleArrayList {
	
	private Integer[] list;
	
	SimpleArrayList(){
		list=new Integer[0];
		}//default constructor set array length to 0
	
	SimpleArrayList(int length){
		list=new Integer[length];
		for(int i=0;i<length;i++) {
			list[i]=0;
		}
		}
		//constructor set length and initialize the Integer array by 0
	
	
	 public void add(Integer i) {
		 Integer[] temp=new Integer[list.length+1];
		 //create a array length is bigger than original length by 1
		 for(int index=0;index<list.length;index++)
			 temp[index]=list[index];
		 //copy the old array to new array
		 temp[list.length]=i;
		 //set the last number of array, and it's the add one
		 list=temp;
	 }
	 
	 public Integer get(int index) {
		 if(index<list.length&&index>=0)
			 return list[index];
		 else return null;
	 }
	 //basically the same as array[index] do, with check of inexistent index, so no risk of NullPointException, but still has to handle null outside
	 
	 public Integer set(int index, Integer element) {
		 if(index<list.length&&index>=0) {
			 Integer temp=list[index];
			 list[index]=element;
			 return temp;
			 }
		 else return null;
	 }
	 //the same as array[index] do ,with check of invalid index
	 
	 public boolean remove(int index) {
		 if(list[index]==null)
			 return false;
		 //if element at index is null, do nothing and return false
		 if(index<list.length&&index>=0) {
			 Integer[] temp=new Integer[list.length-1];
			 for(int i=0;i<index;i++)
				 temp[i]=list[i];
			 //copy the element before index
			 for(int i=index;i<list.length-1;i++)
				 temp[i]=list[i+1];
			 //move all the element after index toward left ,so element at index is deleted
			 list=temp;
			 return true;
		 }
		 else return false;
		 //if index is invalid, return false and do nothing
	 }
	 
	 public void clear() {
		 list=new Integer[0];
	 }
	 //make list's length become 0
	 
	 public int size() {
		 return list.length;
	 }
	 //return list's size
	 
	 public boolean retainAll(SimpleArrayList l) {
		 boolean hadbeenchanged=false;
		 for(int i=0;i<list.length;i++) {
			 boolean flag = false;
			 for(int j=0;j<l.size();j++)
				 if(list[i]==l.get(j)) { //if any element at l equals to list[i], set flag to true and break
					 flag=true;
					 break;
				 }
			 if(!flag) {
				 remove(i);
				 i--;
				 //the i only move toward right, so it must affected by remove(i), minus by one to run correctly
				 hadbeenchanged=true;
			 }
				 
		 }
		 return hadbeenchanged;
	 }
}