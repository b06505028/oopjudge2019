
public class Triangle extends Shape {
	
	Triangle(double length){
		super(length);
	}
	public void setLength(double length) {
		this.length=length;
	};
	
	public double getArea() {
		double area=length*length*Math.sqrt(3)/4;
		return Double.parseDouble(String.format("%.2f", area));
	};
	
	public double getPerimeter() {
		double perimeter=3*length;
		return Double.parseDouble(String.format("%.2f", perimeter));
		
	};
	//about comment of different shapes, please see "shape.java"
}
