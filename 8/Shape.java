public abstract class Shape {
	protected double length;	

	public Shape(double length){
		this.length=length;
	} //length getter
	
	public abstract void setLength(double length);
	//set length
	
	public abstract double getArea();
	//calculate area according to shape's type
	
	public abstract double getPerimeter();
	//calculate perimeter according to shape's type
	
	public String getInfo(){
		return "Area = "+getArea()+
			   ", Perimeter = "+getPerimeter();
	}
	
}