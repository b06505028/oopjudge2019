
public class ShapeFactory {
	public enum Type{Triangle,Square,Circle};
	//declare Type as inner enum of ShapeFactory
	public Shape createShape(ShapeFactory.Type shapeType, double length) {
		switch(shapeType) {
		case Triangle:
			return new Triangle(length);//no "break" because the use of "return" has the same effect
		case Square:
			return new Square(length);
		case Circle:
			return new Circle(length);
		default:
			return null;
		}
	}
	
}
