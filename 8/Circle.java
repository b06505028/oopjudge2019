
public class Circle extends Shape {

	Circle(double length){
		super(length);
	} 
	
	public void setLength(double length) {
		this.length=length;
	}; 
	
	public double getArea() {
		double area=length*length*Math.PI/4;
		return Double.parseDouble(String.format("%.2f", area));
	}; 
	
	public double getPerimeter() {
		double perimeter=length*Math.PI;
		return Double.parseDouble(String.format("%.2f", perimeter));
		
	};
	
	//about comment of different shapes, please see "shape.java"
}
