
public class Square extends Shape{
	Square(double length){
		super(length);
	}
	public void setLength(double length) {
		this.length=length;
	};
	public double getArea() {
		double area=length*length;
		return Double.parseDouble(String.format("%.2f", area));
	};
	public double getPerimeter() {
		double perimeter=4*length;
		return Double.parseDouble(String.format("%.2f", perimeter));
		
	};
	//about comment of different shapes, please see "shape.java"
}
