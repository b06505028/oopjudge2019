
public class IsLeapYear {
	public boolean determine(int a ) {
		if(a%400==0) //if  can be devided by 400
			return true;
		else if(a%100==0) //if  can be devided by 100
			return false;
		else if(a%4==0) //if  can be devided by 4
			return true;
		else return false;
	};
}
