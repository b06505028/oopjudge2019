
public class Pizza {
	Pizza(){
		Size="small";
		NumberOfCheese=NumberOfPepperoni=NumberOfHam=1;
		//constructor with default value
	}
	Pizza(String size,int cheese,int pepperoni,int ham){
		Size=size;
		NumberOfCheese=cheese;
		NumberOfPepperoni=pepperoni;
		NumberOfHam=ham;
	} //constructor that can set value by parameter
	private String Size;
	public String getSize() {
		return Size;
	}
	public void setSize(String size) {
		Size = size;
	}
	private int NumberOfCheese;
	public int getNumberOfCheese() {
		return NumberOfCheese;
	}
	public void setNumberOfCheese(int numberOfCheese) {
		NumberOfCheese = numberOfCheese;
	}
	private int NumberOfPepperoni;
	public int getNumberOfPepperoni() {
		return NumberOfPepperoni;
	}
	public void setNumberOfPepperoni(int numberOfPepperoni) {
		NumberOfPepperoni = numberOfPepperoni;
	}
	private int NumberOfHam;

	public int getNumberOfHam() {
		return NumberOfHam;
	}
	public void setNumberOfHam(int numberOfHam) {
		NumberOfHam = numberOfHam;
	}
	public double calcCost() {
		double cost=0;
		switch(Size) {
		case "small":
			cost=10;break;
		case "medium":
			cost=12;break;
		case "large":
			cost=14;break;
		}
		int count =NumberOfCheese+NumberOfPepperoni+NumberOfHam;
		cost+=+count*2;
		//cost equal to size price add total number of ingredients multiply by two
		return cost;
	}
	public boolean equals(Pizza OtherPizza) {
		if(OtherPizza.getSize()==Size) 
			if(OtherPizza.getNumberOfCheese()==NumberOfCheese)
				if(OtherPizza.getNumberOfPepperoni()==NumberOfPepperoni)
					if(OtherPizza.getNumberOfHam()==NumberOfHam)
						return true;
		//if all number of ingredient are identical, they equal each other
		return false;
	}
	public String toString() {
		String info;
		info="size = "+Size+", numOfCheese = "+NumberOfCheese+", numOfPepperoni = "+NumberOfPepperoni+", numOfHam = "+NumberOfHam;
		return info;
	} 
	//override toString of "Object"
}
