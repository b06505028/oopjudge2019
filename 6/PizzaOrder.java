
public class PizzaOrder {
	private int numberOfPizzas;
	
	private Pizza[] pizza=new Pizza[3];
	//a pizza array of size 3
	
	public boolean setNumberPizzas(int numberPizzas) {
		if(numberPizzas<=3&&numberPizzas>=1) { //if number of pizza is valid
			numberOfPizzas=numberPizzas;
			return true;
		}
		return false;
	}
	public void setPizza1(Pizza pizza1) {
		pizza[0]=new Pizza(pizza1.getSize(),pizza1.getNumberOfCheese(),pizza1.getNumberOfPepperoni(),pizza1.getNumberOfHam());
		//set pizza1
	}
	public void setPizza2(Pizza pizza2) {
		pizza[1]=new Pizza(pizza2.getSize(),pizza2.getNumberOfCheese(),pizza2.getNumberOfPepperoni(),pizza2.getNumberOfHam());
		//set pizza2
	}
	public void setPizza3(Pizza pizza3) {
		pizza[2]=new Pizza(pizza3.getSize(),pizza3.getNumberOfCheese(),pizza3.getNumberOfPepperoni(),pizza3.getNumberOfHam());
		//set pizza3
	}
	public double calcTotal() {
		double cost=0;
		for(int i=0;i<=numberOfPizzas;i++) {
			cost+=pizza[i].calcCost();
			//call calcCost of pizzas to calculate total price
			//total price equals to assumption of individual price
		}
		return cost;
	}
}
