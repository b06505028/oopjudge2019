package loginSystem;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;

public class LoginSystem extends JFrame {

	private JPanel contentPane;
	public LoginPanel loginView;
	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginSystem frame = new LoginSystem();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public LoginSystem() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1920, 1080);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		
		loginView =  new LoginPanel();
		
		add(loginView);
//		ChangePanel changeView = new ChangePanel();
//		add(changeView);
//		ForgetPanel forgetView = new ForgetPanel();
//		add(forgetView);
//		SignupPanel signupView = new SignupPanel();
//		add(signupView);
//		MainpagePanel mainView = new MainpagePanel();
//		add(mainView);
	}

}
