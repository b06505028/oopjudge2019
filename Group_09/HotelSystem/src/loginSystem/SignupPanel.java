package loginSystem;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class SignupPanel extends JPanel {
	
	private JPasswordField passwordText;
	private JPasswordField password2Text;
	Connection connection = null;
	/**
	 * Create the panel.
	 */
	public SignupPanel() {
		
		connection = SQLiteUserConn.UserConnector();
		
		setBounds(0, 0, 1920, 1080);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 0,0));
		panel.setBounds(0, 0, 1920, 1080);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_6 = new JLabel("New label");
		lblNewLabel_6.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblNewLabel_6.setBounds(533, 647, 80, 23);
		panel.add(lblNewLabel_6);
		
		JLabel ldlID = new JLabel("Your ID:");
		ldlID.setForeground(Color.WHITE);
		ldlID.setFont(new Font("High Tower Text", Font.BOLD, 24));
		ldlID.setBackground(Color.WHITE);
		ldlID.setBounds(690, 179, 165, 23);
		panel.add(ldlID);
		
		JLabel lblname = new JLabel("Your name:");
		lblname.setForeground(Color.WHITE);
		lblname.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblname.setBounds(690, 239, 165, 23);
		panel.add(lblname);
		
		JLabel lblphone = new JLabel("Your phone number:");
		lblphone.setForeground(Color.WHITE);
		lblphone.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblphone.setBounds(690, 299, 256, 23);
		panel.add(lblphone);
		
		JLabel lblemail = new JLabel("Your email:");
		lblemail.setForeground(Color.WHITE);
		lblemail.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblemail.setBounds(690, 359, 141, 23);
		panel.add(lblemail);
		
		JTextField IDText = new JTextField();
		IDText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		IDText.setColumns(10);
		IDText.setBounds(940, 176, 300, 29);
		panel.add(IDText);
		
		JTextField nameText = new JTextField();
		nameText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		nameText.setColumns(10);
		nameText.setBounds(940, 236, 300, 29);
		panel.add(nameText);
		
		JTextField phoneText = new JTextField();
		phoneText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		phoneText.setColumns(10);
		phoneText.setBounds(940, 296, 300, 29);
		panel.add(phoneText);
		
		JTextField emailText = new JTextField();
		emailText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		emailText.setColumns(10);
		emailText.setBounds(940, 356, 300, 29);
		panel.add(emailText);
		
		JLabel lblpassword = new JLabel("Password:");
		lblpassword.setForeground(Color.WHITE);
		lblpassword.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblpassword.setBounds(690, 419, 141, 23);
		panel.add(lblpassword);
		
		JLabel lblpassword2 = new JLabel("Varify password again:");
		lblpassword2.setForeground(Color.WHITE);
		lblpassword2.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblpassword2.setBounds(690, 479, 271, 23);
		panel.add(lblpassword2);
		
		JPasswordField password1Text = new JPasswordField();
		password1Text.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		password1Text.setBounds(940, 416, 300, 29);
		panel.add(password1Text);
		
		JPasswordField password2Text = new JPasswordField();
		password2Text.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		password2Text.setBounds(940, 476, 300, 29);
		panel.add(password2Text);
		
		JButton btnHome = new JButton("home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.removeAll();
				LoginPanel loginView = new LoginPanel();
				
				panel.add(loginView);
				panel.repaint();
				panel.revalidate();
			}
		});
		btnHome.setBounds(0, 0, 111, 31);
		panel.add(btnHome);
		
		JLabel lblsameID = new JLabel("The ID was already been used");
		lblsameID.setForeground(Color.RED);
		lblsameID.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblsameID.setBounds(940, 203, 239, 23);
		panel.add(lblsameID);
		lblsameID.setVisible(false);
		
		JLabel lblsamePhone = new JLabel("The phone number has already been used in previous sign up ");
		lblsamePhone.setForeground(Color.RED);
		lblsamePhone.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblsamePhone.setBounds(940, 323, 512, 23);
		panel.add(lblsamePhone);
		lblsamePhone.setVisible(false);
		
		JLabel lblsameEmail = new JLabel("The email address has already been used in previous sign up");
		lblsameEmail.setForeground(Color.RED);
		lblsameEmail.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblsameEmail.setBounds(940, 382, 501, 23);
		panel.add(lblsameEmail);
		lblsameEmail.setVisible(false);
		
		JLabel lbldifferentPassword = new JLabel("The verification password is different from the original one");
		lbldifferentPassword.setForeground(Color.RED);
		lbldifferentPassword.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lbldifferentPassword.setBounds(940, 442, 501, 23);
		panel.add(lbldifferentPassword);
		lbldifferentPassword.setVisible(false);
		
		JLabel lblThereAreEmpty = new JLabel("There are empty blanks, please fill it");
		lblThereAreEmpty.setForeground(Color.RED);
		lblThereAreEmpty.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblThereAreEmpty.setBounds(940, 539, 468, 23);
		panel.add(lblThereAreEmpty);
		lblThereAreEmpty.setVisible(false);
		
		JLabel lblDataSave = new JLabel("Sign up is success,welcome!");
		lblDataSave.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblDataSave.setForeground(Color.GREEN);
		lblDataSave.setBounds(718, 647, 228, 28);
		panel.add(lblDataSave);
		lblDataSave.setVisible(false);
		
		
		JButton button = new JButton("Sign Up");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					
				String userquery = "select * from UserData where Username = ?";
				String phonequery = "select * from UserData where Phone = ?";
				String emailquery = "select * from UserData where Email = ?";
				
				PreparedStatement userpst = connection.prepareStatement(userquery);
				PreparedStatement phonepst = connection.prepareStatement(phonequery);
				PreparedStatement emailpst = connection.prepareStatement(emailquery);
				
				userpst.setString(1, IDText.getText());
				phonepst.setString(1, phoneText.getText());
				emailpst.setString(1, emailText.getText());
				
				ResultSet userresult = userpst.executeQuery();
				ResultSet phoneresult = phonepst.executeQuery();
				ResultSet emailresult = emailpst.executeQuery();
				
				String name = nameText.getText();
				String user = IDText.getText();
				String phone = phoneText.getText();
				String email = emailText.getText();
				String password1 = password1Text.getText();
				String password2 = password2Text.getText();
				
				int count = 0;
				
				if(userresult.next()) {
					lblsameID.setVisible(true);
					count = 1;
				}
				if(phoneresult.next()) {
					lblsamePhone.setVisible(true);
					count = 1;
				}
				if(emailresult.next()) {
					lblsameEmail.setVisible(true);
					count = 1;
				}
				if(!(password1.equals(password2))) {
					lbldifferentPassword.setVisible(true);
					count = 1;
				}
				if(name.length()==0||user.length()==0 ||phone.length()==0|| email.length()==0|| password1.length()==0||password2.length()==0) {
					lblThereAreEmpty.setVisible(true);
					count = 1;
				}
				if(count ==0){
					
					lblsameID.setVisible(false);
					lblsamePhone.setVisible(false);
					lblsameEmail.setVisible(false);
					lbldifferentPassword.setVisible(false);
					lblThereAreEmpty.setVisible(false);
					
					
					String Dataquery = "insert into UserData (Name,Username,Password,Phone,Email) values(?,?,?,?,?)";
					PreparedStatement pst = connection.prepareStatement(Dataquery);
					pst.setString(1, nameText.getText());
					pst.setString(2, IDText.getText());
					pst.setString(3, password1Text.getText());
					pst.setString(4, phoneText.getText());
					pst.setString(5, emailText.getText());
					pst.execute();
					
					
					pst.close();
					lblDataSave.setVisible(true);
					connection.close();
				}
				
			}catch(Exception noconn) {
				
			}
			}
		});
		button.setForeground(Color.GREEN);
		button.setFont(new Font("High Tower Text", Font.BOLD, 24));
		button.setBackground(new Color(0, 0, 0, 75));
		button.setBounds(674, 577, 300, 60);
		panel.add(button);
		

		
		

		
		
		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 1920, 1080);
		panel.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\table16_1920x1440.jpg"));
	}
}
