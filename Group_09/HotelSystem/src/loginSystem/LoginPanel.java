package loginSystem;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import json.BookView;
import json.ChangeView;
import json.CheckView;
import json.HomeView;
import json.SearchView;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class LoginPanel extends JPanel {
	public JTextField usernameText;
	private JPasswordField userpasswordText;
	public HomeView theHomeView;
	public BookView theBookView;
	public ChangeView theChangeView;
	public CheckView theCheckView;
	public SearchView theSearchViewForCheck;
	public SearchView theSearchViewForChange;
	Connection connection = null;
	
	/**
	 * Create the panel.
	 */
	public LoginPanel() {
		theHomeView=new HomeView();
		theBookView=new BookView();
		theChangeView =new ChangeView();
		theCheckView=new CheckView();
		theSearchViewForCheck=new SearchView();
		theSearchViewForChange=new SearchView();
		theHomeView.addLogOutListener(new LogOutListener());
		theBookView.addLogOutListener(new LogOutListener());
		theChangeView.addLogOutListener(new LogOutListener());
		theCheckView.addLogOutListener(new LogOutListener());
		theSearchViewForCheck.addLogOutListener(new LogOutListener());
		theSearchViewForChange.addLogOutListener(new LogOutListener());
		
		setBounds(0, 0, 1920, 1080);
		setLayout(null);
		
		connection = SQLiteUserConn.UserConnector();
		
		
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 0,0));
		panel.setBounds(0, 0, 1920, 1080);
		add(panel);
		panel.setLayout(null);
		
		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setForeground(Color.WHITE);
		lblUsername.setFont(new Font("High Tower Text", Font.BOLD | Font.ITALIC, 24));
		lblUsername.setBounds(1327, 327, 177, 23);
		panel.add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("High Tower Text", Font.BOLD | Font.ITALIC, 24));
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(1327, 440, 113, 23);
		panel.add(lblPassword);
		
		usernameText = new JTextField();
		usernameText.setBounds(1519, 323, 205, 29);
		panel.add(usernameText);
		usernameText.setColumns(10);
		
		userpasswordText = new JPasswordField();
		userpasswordText.setBounds(1521, 434, 205, 29);
		panel.add(userpasswordText);
		
		JLabel lblCorrectPassword = new JLabel("Correct Password!");
		lblCorrectPassword.setBackground(Color.WHITE);
		lblCorrectPassword.setBounds(1560, 466, 142, 22);
		panel.add(lblCorrectPassword);
		lblCorrectPassword.setForeground(Color.GREEN);
		lblCorrectPassword.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblCorrectPassword.setVisible(false);
		
		JLabel lblVaildUsername = new JLabel("Vaild Username");
		lblVaildUsername.setBounds(1554, 352, 129, 22);
		panel.add(lblVaildUsername);
		lblVaildUsername.setForeground(Color.RED);
		lblVaildUsername.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblVaildUsername.setVisible(false);
		
		JLabel lblCorrectUsername = new JLabel("Correct Username!");
		lblCorrectUsername.setBounds(1554, 352, 148, 22);
		panel.add(lblCorrectUsername);
		lblCorrectUsername.setBackground(Color.BLACK);
		lblCorrectUsername.setForeground(Color.GREEN);
		lblCorrectUsername.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblCorrectUsername.setVisible(false);
		
		JLabel lblVaildPassword = new JLabel("Vaild Password");
		lblVaildPassword.setBounds(1560, 466, 123, 22);
		panel.add(lblVaildPassword);
		lblVaildPassword.setForeground(Color.RED);
		lblVaildPassword.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblVaildPassword.setVisible(false);
		
		JButton btnSignIn = new JButton("Sign In");
		btnSignIn.setBackground(Color.BLACK);
		btnSignIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					String query = "select * from UserData where Username=? and Password=? ";
					String usernamequery = "select * from UserData where Username = ?";
					
					PreparedStatement usernamepst = connection.prepareStatement(usernamequery);
					usernamepst.setString(1, usernameText.getText());
					ResultSet userresult = usernamepst.executeQuery();
					
					if(!userresult.next()) {
					
						
						lblCorrectUsername.setVisible(false);
						lblCorrectPassword.setVisible(false);
						lblVaildUsername.setVisible(true);//
						lblVaildPassword.setVisible(false);
						
					}
					else {	
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, usernameText.getText());
					pst.setString(2, userpasswordText.getText());
					ResultSet result = pst.executeQuery();
					
					String inputUser = usernameText.getText();
					String inputPassword = userpasswordText.getText();
					
					if (result.next()) {
						
						lblVaildUsername.setVisible(false);
						lblVaildPassword.setVisible(false);
						lblCorrectUsername.setVisible(true);
						lblCorrectPassword.setVisible(true);
						
						
						theHomeView.setVisible(true);
						theHomeView.setUserLable(inputUser);
						theBookView.setUserLable(inputUser);
						theSearchViewForCheck.setUserLable(inputUser);
						theSearchViewForChange.setUserLable(inputUser);
						theCheckView.setUserLable(inputUser);
						theChangeView.setUserLable(inputUser);
						
						panel.removeAll();
						
						
						//frame.setVisible(false);
						
						panel.repaint();
						panel.revalidate();
						connection.close();
						
						
					}
					
					else {
						lblCorrectUsername.setVisible(false);
						lblCorrectPassword.setVisible(false);
						lblVaildUsername.setVisible(true);
						lblVaildPassword.setVisible(true);
						
						
						}
					}
						
					
				}catch(Exception noconn) {
					JOptionPane.showMessageDialog(null, noconn);
					
				}
				
				
			}
		});
		btnSignIn.setFont(new Font("High Tower Text", Font.BOLD | Font.ITALIC, 24));
		btnSignIn.setContentAreaFilled(false);
		btnSignIn.setBackground(Color.BLACK);
		btnSignIn.setForeground(Color.WHITE);
		btnSignIn.setBounds(1494, 564, 205, 31);
		panel.add(btnSignIn);
		
		JButton btnChange = new JButton("Change User Data");
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String query = "select * from UserData where Username=? and Password=? ";
					String usernamequery = "select * from UserData where Username = ?";
					
					PreparedStatement usernamepst = connection.prepareStatement(usernamequery);
					usernamepst.setString(1, usernameText.getText());
					ResultSet userresult = usernamepst.executeQuery();
					
					if(!userresult.next()) {
					
						
						lblCorrectUsername.setVisible(false);
						
						lblCorrectPassword.setVisible(false);
						
						lblVaildUsername.setVisible(true);//
						userresult.close();
						usernamepst.close();
					}
					
					else {	
					PreparedStatement pst = connection.prepareStatement(query);
					pst.setString(1, usernameText.getText());
					pst.setString(2, userpasswordText.getText());
					ResultSet result = pst.executeQuery();
					
					String inputUser = usernameText.getText();
					String inputPassword = userpasswordText.getText();
					
					if (result.next()) {
						lblVaildUsername.setVisible(false);
						lblVaildPassword.setVisible(false);
						lblCorrectUsername.setVisible(true);
						lblCorrectPassword.setVisible(true);

						loginUserIs RememberUser = new  loginUserIs();
						RememberUser.setUser(inputUser);
						
//						MainPageView mainpage = new MainPageView();
//						mainpage.setVisible(true);
						panel.removeAll();
						ChangePanel changeView = new ChangePanel();
						panel.add(changeView);
						panel.repaint();
						panel.revalidate();
						
						result.close();
						pst.close();
						
						connection.close();
					}
					
					else {
						lblCorrectUsername.setVisible(false);
						lblCorrectPassword.setVisible(false);
						lblVaildUsername.setVisible(true);
						lblVaildPassword.setVisible(true);
						
						result.close();
						pst.close();
						}
					}
						
					
				}catch(Exception noconn) {
					JOptionPane.showMessageDialog(null, noconn);
					
				}
				
			}
		});
		btnChange.setFont(new Font("High Tower Text", Font.BOLD | Font.ITALIC, 24));
		btnChange.setContentAreaFilled(false);
		btnChange.setBackground(Color.BLACK);
		btnChange.setForeground(Color.WHITE);
		btnChange.setBounds(1494, 632, 268, 31);
		panel.add(btnChange);
		
		JButton btnSignUp = new JButton("Haven't Sign Up? Click me...");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panel.removeAll();
				SignupPanel signupView = new SignupPanel();
				panel.add(signupView);
				panel.repaint();
				panel.revalidate();
			}
		});
		btnSignUp.setForeground(Color.WHITE);
		btnSignUp.setFont(new Font("High Tower Text", Font.BOLD | Font.ITALIC, 18));
		btnSignUp.setContentAreaFilled(false);
		btnSignUp.setBackground(Color.BLACK);
		btnSignUp.setBounds(1494, 812, 268, 31);
		panel.add(btnSignUp);
		
		JButton btnForget = new JButton("Forget Password? Click me...");
		btnForget.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				panel.removeAll();
				ForgetPanel forgetView = new ForgetPanel();
				panel.add(forgetView);
				panel.repaint();
				panel.revalidate();
				
			}
		});
		
		btnForget.setForeground(Color.WHITE);
		btnForget.setFont(new Font("High Tower Text", Font.BOLD | Font.ITALIC, 18));
		btnForget.setContentAreaFilled(false);
		btnForget.setBounds(1494, 738, 268, 31);
		btnForget.setBackground(new Color(0, 0, 0, 180));
		panel.add(btnForget);
		
		JLabel block = new JLabel("");
		block.setBounds(1239, 5, 681, 1049);
		block.setBackground(new Color(0, 0, 0, 180));
		block.setOpaque(true);
		panel.add(block);
		

		

		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 1920, 1288);
		panel.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\loginView.png"));
		
	}
	public class LogOutListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			theHomeView.setVisible(false);
			theSearchViewForCheck.setVisible(false);
			theSearchViewForChange.setVisible(false);
			theBookView.setVisible(false);
			theCheckView.setVisible(false);
			theChangeView.setVisible(false);
			
			LoginSystem frame = new LoginSystem();
			frame.setVisible(true);
			//LoginPanel loginView = new LoginPanel();
			//loginView.setVisible(true);
			//add(loginView);
			repaint();
			revalidate();
			
		}
		
	}
}
