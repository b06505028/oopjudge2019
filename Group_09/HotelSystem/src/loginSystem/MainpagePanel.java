package loginSystem;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainpagePanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public MainpagePanel() {
		setBounds(0, 0, 1920, 1080);
		setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1920, 1080);
		panel.setBackground(new Color(0, 0, 0,0));
		add(panel);
		panel.setLayout(null);
		
		JButton btnHome = new JButton("home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.removeAll();
				LoginPanel loginView = new LoginPanel();
				
				panel.add(loginView);
				panel.repaint();
				panel.revalidate();
			}
		});
		btnHome.setBounds(0, 0, 111, 31);
		panel.add(btnHome);
		
		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 1920, 1080);
		panel.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\SignUp.jpg"));
	}
}
