package loginSystem;

import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ForgetPanel extends JPanel {

	Connection connection = null;
	private JTextField IDText;
	private JTextField NameText;
	private JTextField PhoneText;
	private JTextField EmailText;
	private JTextField Password2Text;
	private JTextField Password1Text;
	/**
	 * Create the panel.
	 */
	public ForgetPanel() {
		
		setBounds(0, 0, 1920, 1080);
		setLayout(null);
		
		connection = SQLiteUserConn.UserConnector();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 0, 0,0));
		panel.setBounds(0, 0, 1920, 1080);
		add(panel);
		panel.setLayout(null);
		
		IDText = new JTextField();
		IDText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		IDText.setColumns(10);
		IDText.setBounds(972, 247, 300, 29);
		panel.add(IDText);
		
		NameText = new JTextField();
		NameText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		NameText.setColumns(10);
		NameText.setBounds(972, 327, 300, 29);
		panel.add(NameText);
		
		JButton btnHome = new JButton("Home");
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panel.removeAll();
				LoginPanel loginView = new LoginPanel();
				
				panel.add(loginView);
				panel.repaint();
				panel.revalidate();
			}
		});
		btnHome.setBounds(0, 0, 111, 31);
		panel.add(btnHome);
		
		PhoneText = new JTextField();
		PhoneText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		PhoneText.setColumns(10);
		PhoneText.setBounds(972, 407, 300, 29);
		panel.add(PhoneText);
		
		EmailText = new JTextField();
		EmailText.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		EmailText.setColumns(10);
		EmailText.setBounds(972, 487, 300, 29);
		panel.add(EmailText);
		
		JLabel lblID = new JLabel("Your ID:");
		lblID.setForeground(Color.WHITE);
		lblID.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblID.setBackground(Color.WHITE);
		lblID.setBounds(650, 250, 165, 23);
		panel.add(lblID);
		
		JLabel lblName = new JLabel("Your name:");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblName.setBounds(650, 330, 165, 23);
		panel.add(lblName);
		
		JLabel lblPhone = new JLabel("Your phone number:");
		lblPhone.setForeground(Color.WHITE);
		lblPhone.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblPhone.setBounds(650, 410, 256, 23);
		panel.add(lblPhone);
		
		JLabel lblEmail = new JLabel("Your email:");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblEmail.setBounds(650, 490, 141, 23);
		panel.add(lblEmail);
		
		JLabel lblPassword1 = new JLabel("New Password:");
		lblPassword1.setForeground(Color.WHITE);
		lblPassword1.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblPassword1.setBounds(650, 720, 233, 23);
		panel.add(lblPassword1);
		lblPassword1.setVisible(false);
		
		
		JLabel lblPassword2 = new JLabel("Varify New password again:");
		lblPassword2.setForeground(Color.WHITE);
		lblPassword2.setFont(new Font("High Tower Text", Font.BOLD, 24));
		lblPassword2.setBounds(594, 800, 361, 23);
		panel.add(lblPassword2);
		lblPassword2.setVisible(false);
		
		Password2Text = new JTextField();
		Password2Text.setBounds(972, 797, 300, 29);
		panel.add(Password2Text);
		Password2Text.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		Password2Text.setColumns(10);
		Password2Text.setVisible(false);
		
		Password1Text = new JTextField();
		Password1Text.setBounds(972, 717, 300, 29);
		panel.add(Password1Text);
		Password1Text.setFont(new Font("High Tower Text", Font.PLAIN, 24));
		Password1Text.setColumns(10);
		Password1Text.setVisible(false);
		

		
		JLabel lblEmpty1 = new JLabel("There are empty blanks, please fill it");
		lblEmpty1.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblEmpty1.setForeground(Color.RED);
		lblEmpty1.setBackground(Color.BLACK);
		lblEmpty1.setBounds(972, 554, 316, 23);
		panel.add(lblEmpty1);
		lblEmpty1.setVisible(false);
		
		JLabel lblCannotMatch = new JLabel("Sorry, we cannot find any matched user data");
		lblCannotMatch.setForeground(Color.RED);
		lblCannotMatch.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblCannotMatch.setBounds(972, 578, 349, 23);
		panel.add(lblCannotMatch);
		lblCannotMatch.setVisible(false);
		
		JLabel lbldifferentPassword = new JLabel("The verification password is different from the original one");
		lbldifferentPassword.setForeground(Color.RED);
		lbldifferentPassword.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lbldifferentPassword.setBounds(972, 856, 498, 23);
		panel.add(lbldifferentPassword);
		lbldifferentPassword.setVisible(false);
		
		JLabel lblDataSave = new JLabel("User Data Save Successfully");
		lblDataSave.setForeground(Color.GREEN);
		lblDataSave.setFont(new Font("High Tower Text", Font.PLAIN, 18));
		lblDataSave.setBounds(950, 973, 349, 23);
		panel.add(lblDataSave);
		lblDataSave.setVisible(false);
		
		JButton btnNewButton = new JButton("Change Password!");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String user = IDText.getText();
				String password1 = Password1Text.getText();
				String password2 = Password2Text.getText();
				
				
				if(!(password1.equals(password2))) {
					lbldifferentPassword.setVisible(true);
					
				}
				else {
					String Dataquery = "Update UserData Set Password ='"+Password1Text.getText()+"' where Username = '"+user+"'";
					PreparedStatement pst;
					try {
						
						pst = connection.prepareStatement(Dataquery);
						
						pst.executeUpdate();
						lbldifferentPassword.setVisible(false);
						lblDataSave.setVisible(true);
						pst.close();
						connection.close();
						
						
						
					} catch (SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					};
					
				}
				
				
				
			}
		});
		btnNewButton.setBounds(950, 917, 165, 31);
		panel.add(btnNewButton);
		btnNewButton.setVisible(false);
		
		JButton btnFindMe = new JButton("Find Me");
		btnFindMe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {		
				try {
					lblEmpty1.setVisible(false);
					lblCannotMatch.setVisible(true);
					
					String userquery = "select * from UserData where Username = ?";
					String namequery = "select * from UserData where Name = ?";
					String phonequery = "select * from UserData where Phone = ?";
					String emailquery = "select * from UserData where Email = ?";
					
					String user = IDText.getText();
					String name = NameText.getText();
					String phone = PhoneText.getText();
					String email = EmailText.getText();
					
					PreparedStatement userpst = connection.prepareStatement(userquery);
					userpst.setString(1,IDText.getText() );
					ResultSet userresult =userpst.executeQuery();
					if(name.length()==0||phone.length()==0|| email.length()==0||user.length() ==0) {
						lblEmpty1.setVisible(true);
					}
					else if(userresult.next()) {
						
						userresult.close();
						userpst.close();
						
						PreparedStatement namepst = connection.prepareStatement(namequery);
						namepst.setString(1,NameText.getText());
						ResultSet nameresult =namepst.executeQuery();
						if(nameresult.next()) {
							nameresult.close();
							namepst.close();
							
							PreparedStatement phonepst = connection.prepareStatement(phonequery);
							phonepst.setString(1,PhoneText.getText() );
							ResultSet phoneresult =phonepst.executeQuery();
							if(phoneresult.next()) {
								phoneresult.close();
								phonepst.close();
								
								PreparedStatement emailpst = connection.prepareStatement(emailquery);
								emailpst.setString(1,EmailText.getText() );
								ResultSet emailresult =emailpst.executeQuery();
								if(emailresult.next()) {
									
									emailresult.close();
									emailpst.close();
									
									lblCannotMatch.setVisible(false);
									
									IDText.setEnabled(false);
									NameText.setEnabled(false);
									PhoneText.setEnabled(false);
									EmailText.setEnabled(false);
									
									Password1Text.setVisible(true);
									Password2Text.setVisible(true);
									lblPassword1.setVisible(true);
									lblPassword2.setVisible(true);
									btnNewButton.setVisible(true);
								}
							}
						}
						
						
						
						
					}
					else {
							lblCannotMatch.setVisible(true);
						}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnFindMe.setBounds(950, 616, 165, 31);
		panel.add(btnFindMe);
		

		
	
		
		
		
	
		
		JLabel lblblock = new JLabel("");
		lblblock.setBounds(410, 0, 1100, 1080);
		lblblock.setBackground(new Color(0, 0, 0, 180));
		lblblock.setOpaque(true); 
		panel.add(lblblock);
		
		
		JLabel lblbackground = new JLabel("");
		lblbackground.setBounds(0, 0, 1920, 1080);
		panel.add(lblbackground);
		lblbackground.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\photo-1532516993453-addb6c882c42.jpg"));
		
	}
}
