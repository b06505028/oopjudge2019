package json;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.ImageIcon;

public class ChangeView extends JFrame {

	private ButtonGroup group;
	private JPanel contentPane;
	private JRadioButton changeRoomNumberRadioButton;
	private JRadioButton changeDateRadioButton;
	private JTextField dayIn;
	private JTextField dayOut;
	private JButton changeDateButton;
	private JButton changeRoomNumberButton;
	private JSpinner singleRoomSpinner;
	private JSpinner doubleRoomSpinner;
	private JSpinner quadRoomSpinner;
	private JButton homeButton;
	private JButton deleteButton;
	private JLabel userLable;
	private JButton logOutButton;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChangeView frame = new ChangeView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/

	public ChangeView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		contentPane = new JPanel();
		setBounds(0, 0, 1920, 1080);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("\u55AE\u4EBA\u623F");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label.setBounds(806, 333, 84, 28);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("\u96D9\u4EBA\u623F");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_1.setBounds(916, 333, 93, 28);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("\u56DB\u4EBA\u623F");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_2.setBounds(1023, 333, 99, 28);
		contentPane.add(label_2);
		
		singleRoomSpinner = new JSpinner();
		singleRoomSpinner.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		singleRoomSpinner.setEnabled(false);
		singleRoomSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		singleRoomSpinner.setBounds(812, 374, 51, 40);
		contentPane.add(singleRoomSpinner);
		
		doubleRoomSpinner = new JSpinner();
		doubleRoomSpinner.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		doubleRoomSpinner.setEnabled(false);
		doubleRoomSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		doubleRoomSpinner.setBounds(922, 374, 51, 40);
		contentPane.add(doubleRoomSpinner);
		
		quadRoomSpinner = new JSpinner();
		quadRoomSpinner.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		quadRoomSpinner.setEnabled(false);
		quadRoomSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		quadRoomSpinner.setBounds(1029, 374, 51, 40);
		contentPane.add(quadRoomSpinner);
		
		changeRoomNumberButton = new JButton("\u4FEE\u6539");
		changeRoomNumberButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		changeRoomNumberButton.setEnabled(false);
		changeRoomNumberButton.setBounds(1223, 366, 99, 27);
		contentPane.add(changeRoomNumberButton);
		
		changeRoomNumberRadioButton = new JRadioButton("\u6539\u52D5\u623F\u9593\u6578\u91CF");
		changeRoomNumberRadioButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		changeRoomNumberRadioButton.setBounds(586, 373, 200, 40);
		contentPane.add(changeRoomNumberRadioButton);
		
		changeDateRadioButton = new JRadioButton("\u66F4\u6539\u65E5\u671F");
		changeDateRadioButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		changeDateRadioButton.setBounds(586, 596, 200, 40);
		contentPane.add(changeDateRadioButton);
		
		changeRoomNumberRadioButton.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					singleRoomSpinner.setEnabled(true);
					doubleRoomSpinner.setEnabled(true);
					quadRoomSpinner.setEnabled(true);
					changeRoomNumberButton.setEnabled(true);
					
			    }
			    else if (e.getStateChange() == ItemEvent.DESELECTED) {
			    	singleRoomSpinner.setEnabled(false);
					doubleRoomSpinner.setEnabled(false);
					quadRoomSpinner.setEnabled(false);
					changeRoomNumberButton.setEnabled(false);
			    }
				
			}
			
		});
		
		changeDateRadioButton.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					dayIn.setEnabled(true);
					dayOut.setEnabled(true);
					changeDateButton.setEnabled(true);
					
			    }
			    else if (e.getStateChange() == ItemEvent.DESELECTED) {
			    	dayIn.setEnabled(false);
			    	dayOut.setEnabled(false);
			    	changeDateButton.setEnabled(false);
			    }
			
			}
			
		});
		
		group = new ButtonGroup();
		group.add(changeRoomNumberRadioButton);
		group.add(changeDateRadioButton);
		
		JLabel label_3 = new JLabel("\u5165\u4F4F\u65E5\u671F");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_3.setBounds(816, 555, 101, 34);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("\u9000\u623F\u65E5\u671F");
		label_4.setForeground(Color.WHITE);
		label_4.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_4.setBounds(968, 555, 99, 34);
		contentPane.add(label_4);
		
		dayIn = new JTextField();
		dayIn.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		dayIn.setEnabled(false);
		dayIn.setBounds(816, 602, 99, 40);
		contentPane.add(dayIn);
		dayIn.setColumns(4);
		
		dayOut = new JTextField();
		dayOut.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		dayOut.setEnabled(false);
		dayOut.setColumns(4);
		dayOut.setBounds(968, 602, 99, 40);
		contentPane.add(dayOut);
		
		changeDateButton = new JButton("\u4FEE\u6539");
		changeDateButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		changeDateButton.setEnabled(false);
		changeDateButton.setBounds(1223, 608, 99, 27);
		contentPane.add(changeDateButton);
		
		userLable = new JLabel();
		userLable.setForeground(Color.WHITE);
		userLable.setFont(new Font("新細明體", Font.PLAIN, 24));
		userLable.setBounds(1559, 13, 150, 46);
		contentPane.add(userLable);
		
		homeButton = new JButton("\u56DE\u4E3B\u9801");
		homeButton.setBounds(0, 0, 136, 46);
		contentPane.add(homeButton);
		
		deleteButton = new JButton("\u53D6\u6D88\u8A02\u55AE");
		deleteButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		deleteButton.setBounds(848, 850, 173, 46);
		contentPane.add(deleteButton);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(539, 0, 874, 1020);
		lblNewLabel_1.setBackground(new Color(0, 0, 0, 180));
		lblNewLabel_1.setOpaque(true);
		contentPane.add(lblNewLabel_1);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\photo-1503505344463-859206198e4b.jpg"));
		lblNewLabel.setBounds(0, 0, 1918, 1020);
		contentPane.add(lblNewLabel);
		
		logOutButton = new JButton("\u767B\u51FA");
		logOutButton.setBounds(1745, 0, 173, 64);
		contentPane.add(logOutButton);
		
	}
	public int getSingleRoom() {
		return (Integer) singleRoomSpinner.getValue();
	}
	public void setSingleRoom(int room) {
		singleRoomSpinner.setValue(room);
	}
	
	public int getDoubleRoom() {
		return (Integer) doubleRoomSpinner.getValue();
	}
	
	public void setDoubleRoom(int room) {
		doubleRoomSpinner.setValue(room);
	}

	public int getQuadRoom() {
		return (Integer) quadRoomSpinner.getValue();
	}
	
	public void setQuadRoom(int room) {
		quadRoomSpinner.setValue(room);
	}
	
	public int getDayIn() {
		return Integer.parseInt(dayIn.getText());
	}
	
	public void setDayIn(Integer day) {
		dayIn.setText(day.toString());
	}
	
	public int getDayOut() {
		return Integer.parseInt(dayOut.getText());
	}
	
	public void setDayOut(Integer day) {
		dayOut.setText(day.toString());
	}
	
	
	public void addChangeRoomNumberButtonListener(ActionListener listener) {
		changeRoomNumberButton.addActionListener(listener);
	}
	
	public void addChangeDateButtonListener(ActionListener listener) {
		changeDateButton.addActionListener(listener);
	}
	public void addHomeButtonListener(ActionListener listener) {
		homeButton.addActionListener(listener);
		
	}
	public void addDeleteButtonListener(ActionListener listener) {
		deleteButton.addActionListener(listener);
	}

	public void clear() {
		dayIn.setText("");;
		dayOut.setText("");;
		singleRoomSpinner.setValue(0);
		doubleRoomSpinner.setValue(0);
		quadRoomSpinner.setValue(0);
	}
	
	public void setUserLable(String userName) {
		userLable.setText("使用者 : "+userName);
	}
	
	public void addLogOutListener(ActionListener listener) {
		logOutButton.addActionListener(listener);
	}
}
