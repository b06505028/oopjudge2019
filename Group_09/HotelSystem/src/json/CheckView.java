package json;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import helpingClasses.HotelOrder;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.ImageIcon;

public class CheckView extends JFrame {

	private JPanel contentPane;
	private JLabel hotelID;
	private JLabel hotelAddress;
	private JLabel dayIn;
	private JLabel dayOut;
	private JLabel hotelStar;
	private JLabel serialNumber;
	private JLabel totalPrice;
	private JLabel hotelLocation;
	private JLabel singleRoomPrice;
	private JLabel doubleRoomPrice;
	private JLabel quadRoomPrice;
	private JLabel singleRoomNumber;
	private JLabel doubleRoomNumber;
	private JLabel quadRoomNumber;
	private JButton homeButton;
	private JLabel userLable;
	private JButton logOutButton;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderCheckDialog frame = new OrderCheckDialog(new HotelOrder());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public CheckView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		setBounds(0, 0, 1920, 1080);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		hotelID = new JLabel("飯店ID : ");
		hotelID.setForeground(Color.WHITE);
		hotelID.setFont(new Font("新細明體", Font.PLAIN, 15));
		hotelID.setBounds(752, 216, 220, 30);
		contentPane.add(hotelID);
		
		hotelAddress = new JLabel("地址 : ");
		hotelAddress.setForeground(Color.WHITE);
		hotelAddress.setFont(new Font("新細明體", Font.PLAIN, 15));
		hotelAddress.setBounds(752, 348, 220, 30);
		contentPane.add(hotelAddress);
		
		dayIn = new JLabel("入住日期 : 2019 / 6 / ");
		dayIn.setForeground(Color.WHITE);
		dayIn.setFont(new Font("新細明體", Font.PLAIN, 15));
		dayIn.setBounds(920, 547, 150, 30);
		contentPane.add(dayIn);
		
		dayOut = new JLabel("退房日期 : 2019 / 6 / ");
		dayOut.setForeground(Color.WHITE);
		dayOut.setFont(new Font("新細明體", Font.PLAIN, 15));
		dayOut.setBounds(920, 582, 150, 30);
		contentPane.add(dayOut);
		
		hotelStar = new JLabel("星級 : ");
		hotelStar.setForeground(Color.WHITE);
		hotelStar.setFont(new Font("新細明體", Font.PLAIN, 15));
		hotelStar.setBounds(752, 287, 220, 30);
		contentPane.add(hotelStar);
		
		serialNumber = new JLabel("訂房代號 : ");
		serialNumber.setForeground(Color.WHITE);
		serialNumber.setFont(new Font("新細明體", Font.PLAIN, 15));
		serialNumber.setBounds(1130, 547, 150, 30);
		contentPane.add(serialNumber);
		
		totalPrice = new JLabel("總金額 : ");
		totalPrice.setForeground(Color.WHITE);
		totalPrice.setFont(new Font("新細明體", Font.PLAIN, 15));
		totalPrice.setBounds(1130, 582, 150, 30);
		contentPane.add(totalPrice);
		
		hotelLocation = new JLabel("地點 : ");
		hotelLocation.setForeground(Color.WHITE);
		hotelLocation.setFont(new Font("新細明體", Font.PLAIN, 15));
		hotelLocation.setBounds(752, 413, 220, 30);
		contentPane.add(hotelLocation);
		
		singleRoomPrice = new JLabel("單人房 : "+" NTD /每間");
		singleRoomPrice.setForeground(Color.WHITE);
		singleRoomPrice.setFont(new Font("新細明體", Font.PLAIN, 15));
		singleRoomPrice.setBounds(1078, 224, 220, 30);
		contentPane.add(singleRoomPrice);
		
		doubleRoomPrice = new JLabel("雙人房 : "+" NTD /每間");
		doubleRoomPrice.setForeground(Color.WHITE);
		doubleRoomPrice.setFont(new Font("新細明體", Font.PLAIN, 15));
		doubleRoomPrice.setBounds(1078, 287, 220, 30);
		contentPane.add(doubleRoomPrice);
		
		quadRoomPrice = new JLabel("四人房 : "+" NTD /每間");
		quadRoomPrice.setForeground(Color.WHITE);
		quadRoomPrice.setFont(new Font("新細明體", Font.PLAIN, 15));
		quadRoomPrice.setBounds(1078, 348, 220, 30);
		contentPane.add(quadRoomPrice);
		
		singleRoomNumber = new JLabel("單人房 : "+" 間");
		singleRoomNumber.setForeground(Color.WHITE);
		singleRoomNumber.setFont(new Font("新細明體", Font.PLAIN, 15));
		singleRoomNumber.setBounds(741, 547, 150, 30);
		contentPane.add(singleRoomNumber);
		
		doubleRoomNumber = new JLabel("雙人房 : "+" 間");
		doubleRoomNumber.setForeground(Color.WHITE);
		doubleRoomNumber.setFont(new Font("新細明體", Font.PLAIN, 15));
		doubleRoomNumber.setBounds(741, 582, 150, 30);
		contentPane.add(doubleRoomNumber);
		
		quadRoomNumber = new JLabel("四人房 : "+" 間");
		quadRoomNumber.setForeground(Color.WHITE);
		quadRoomNumber.setFont(new Font("新細明體", Font.PLAIN, 15));
		quadRoomNumber.setBounds(741, 617, 150, 30);
		contentPane.add(quadRoomNumber);
		
		JLabel lblNewLabel = new JLabel("_____________________________________________________________________________");
		lblNewLabel.setForeground(Color.LIGHT_GRAY);
		lblNewLabel.setBounds(704, 465, 558, 19);
		contentPane.add(lblNewLabel);
		
		homeButton = new JButton("\u78BA\u8A8D");
		homeButton.setBounds(920, 700, 161, 58);
		contentPane.add(homeButton);
		
		logOutButton = new JButton("\u767B\u51FA");
		logOutButton.setBounds(1733, 0, 169, 58);
		contentPane.add(logOutButton);
		
		lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(645, 0, 697, 1033);
		lblNewLabel_2.setBackground(new Color(0, 0, 0, 180));
		lblNewLabel_2.setOpaque(true);
		contentPane.add(lblNewLabel_2);
		
		userLable = new JLabel();
		userLable.setForeground(Color.WHITE);
		userLable.setFont(new Font("新細明體", Font.PLAIN, 24));
		userLable.setBounds(1517, 0, 213, 58);
		contentPane.add(userLable);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\taiwan_sunset_at_jingzaijiao_sunset_landscape_87328_1920x1080.jpg"));
		lblNewLabel_1.setBounds(0, 0, 1902, 1033);
		contentPane.add(lblNewLabel_1);
		
	}
	public void set(HotelOrder order) {
		hotelID.setText("飯店ID : "+order.getID());
		hotelAddress.setText("地址 : "+order.getAddress());
		dayIn.setText("入住日期 : 2019 / 6 / "+order.getDayIn());
		dayOut.setText("退房日期 : 2019 / 6 / "+order.getDayOut());
		hotelStar.setText("星級 : "+order.getHotelStar());
		serialNumber.setText("訂房代號 : "+order.getSerialNumber());
		totalPrice.setText("總金額 : "+order.getTotalPrice()+" NTD");
		hotelLocation.setText("地點 : "+order.getLocation());
		singleRoomPrice.setText("單人房 : "+order.getSingleRoomPrice()+" NTD /每間");
		doubleRoomPrice.setText("雙人房 : "+order.getDoubleRoomPrice()+" NTD /每間");
		quadRoomPrice.setText("四人房 : "+order.getQuadRoomPrice()+" NTD /每間");
		singleRoomNumber.setText("單人房 : "+order.getSingleRoomRequired()+" 間");
		doubleRoomNumber.setText("雙人房 : "+order.getDoubleRoomRequired()+" 間");
		quadRoomNumber.setText("四人房 : "+order.getQuadRoomRequired()+" 間");
		
	}
	public void addHomeButtonListener(ActionListener listener) {
		homeButton.addActionListener(listener);
	}
	
	public void setUserLable(String userName) {
		userLable.setText("使用者 : "+userName);
	}
	
	public void addLogOutListener(ActionListener listener) {
		logOutButton.addActionListener(listener);
	}
}
