package json;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import exceptions.DateExcessException;
import exceptions.DateinValidException;
import exceptions.HotelNotSelectedException;
import exceptions.NoMatchOrderException;
import exceptions.NotEnoughVacancyException;
import exceptions.NumberGenerateErrorException;
import helpingClasses.HotelOrder;
import helpingClasses.HotelRoomTable;
import loginSystem.LoginPanel;
import loginSystem.LoginSystem;

public class Controller {
	
	private HomeView homeView;
	
	private BookView bookView;
	
	private SearchView searchViewForCheck;
	private SearchView searchViewForChange;
	private ChangeView changeView;
	
	private CheckView checkView;
	
	private BookModel bookModel;
	private CheckChangeModel checkChangeModel;
	
	private LoginSystem loginSystem;
	

	public Controller(HotelRoomTable hotelRoomTable, ArrayList<HotelOrder> orderList)
			throws ClassNotFoundException, IOException {
		//System.out.println("test");
		loginSystem=new LoginSystem();
		
		//homeView = new HomeView();
		homeView=loginSystem.loginView.theHomeView;
		homeView.addGoBookButtonListener(new GoBookButtonListener());
		homeView.addGoChangeButtonListener(new GoBookChangeButtonListener());
		homeView.addGoCheckButtonListener(new GoCheckButtonListener());
		System.out.println("test");
		//bookView = new BookView();
		bookView=loginSystem.loginView.theBookView;
		bookView.addBookingButtonListener(new BookingButtonListener());
		bookView.addSearchingListener(new SearchingButtonListener());
		bookView.addHomeButtonListener(new GoHomeListener());

		//searchViewForCheck = new SearchView();
		searchViewForCheck=loginSystem.loginView.theSearchViewForCheck;
		searchViewForCheck.addButtonListener(new SearchToCheckListener());
		searchViewForCheck.addHomeButtonListener(new GoHomeListener());
		
		//searchViewForChange = new SearchView();
		searchViewForChange=loginSystem.loginView.theSearchViewForChange;
		searchViewForChange.addButtonListener(new SearchToChangeListener());
		searchViewForChange.addHomeButtonListener(new GoHomeListener());
		
		//changeView = new ChangeView();
		changeView=loginSystem.loginView.theChangeView;
		changeView.addChangeDateButtonListener(new ChangeDateListener());
		changeView.addChangeRoomNumberButtonListener(new ChangeRoomNumberListener());
		changeView.addHomeButtonListener(new GoHomeListener());
		changeView.addDeleteButtonListener(new DeleteButtonListener());
		
		//checkView=new CheckView();
		checkView=loginSystem.loginView.theCheckView;
		checkView.addHomeButtonListener(new GoHomeListener());
		
		bookModel = new BookModel(hotelRoomTable, orderList);
		checkChangeModel = new CheckChangeModel(hotelRoomTable, orderList);
		loginSystem.setVisible(true);
		
		//homeView.setVisible(true);
	}
	
	//HomeView Begins
	public class GoBookButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			bookView.setVisible(true);
			homeView.setVisible(false);
		}
	}
	
	public class GoBookChangeButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			searchViewForChange.setVisible(true);
			homeView.setVisible(false);
		}
	}

	public class GoCheckButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			searchViewForCheck.setVisible(true);
			homeView.setVisible(false);
		}
	}
	//HomeView Ends
	
	//BookView Begins
	public class BookingButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			HotelOrder hotelSelected = bookView.getSelectedHotel();
			try {
				if (hotelSelected == null)
					throw new HotelNotSelectedException();
				String message = "<html>飯店ID: " + hotelSelected.getID() + "<br/>單人房："
						+ hotelSelected.getSingleRoomRequired() + " 間<br/>雙人房" + hotelSelected.getDoubleRoomRequired()
						+ " 間<br/>四人房" + hotelSelected.getQuadRoomRequired() + " 間<br/>總金額："
						+ hotelSelected.getTotalPrice() + "<br/>確定訂房?";
				int yesOrNo = JOptionPane.showConfirmDialog(bookView, message, "訂房確認", JOptionPane.YES_NO_OPTION);

				if (yesOrNo == 0) {
					bookModel.addOrder(hotelSelected);
					// bookView.clearSelect();
					// bookView.clearList();
					JOptionPane.showMessageDialog(bookView, "訂位代號：" + hotelSelected.getSerialNumber(), "訂房成功", 1);
					System.out.println("Booking succeeded");
				}
			} catch (NumberGenerateErrorException e) {
				JOptionPane.showMessageDialog(bookView, "編號產生錯誤", "訂房錯誤", 2);
			} catch (HotelNotSelectedException e1) {
				JOptionPane.showMessageDialog(bookView, "請選擇方案", "訂房錯誤", 2);
			} catch (NotEnoughVacancyException e2) {
				JOptionPane.showMessageDialog(bookView, "房型數量不足", "訂房錯誤", 2);
			} catch (Exception e3) {
				JOptionPane.showMessageDialog(bookView, "未知的錯誤", "訂房錯誤", 2);

			}
		}

	}

	public class SearchingButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			try {

				if (bookView.getDayIn() < 1 || bookView.getDayIn() > 30 || bookView.getDayOut() < 1
						|| bookView.getDayOut() > 30 || bookView.getDayIn() >= bookView.getDayOut())
					throw new DateinValidException();
				if(bookView.getDayIn()<ZonedDateTime.now().getDayOfMonth())
					throw new DateExcessException();
				bookModel.search(bookView.getStar(), bookView.getSingleRoom(), bookView.getDoubleRoom(),
						bookView.getQuadRoom(), bookView.getDayIn(), bookView.getDayOut());
				ArrayList<HotelOrder> hotelList = bookModel.search(bookView.getStar(), bookView.getSingleRoom(),
						bookView.getDoubleRoom(), bookView.getQuadRoom(), bookView.getDayIn(), bookView.getDayOut());
				bookView.clearSelect();
				bookView.clearList();
				// System.out.println(hotelList.size());
				for (int i = 0; i < hotelList.size(); i++) {
					// System.out.printf("%d / %d\n",i,hotelList.size()-1);
					bookView.addHotel(hotelList.get(i));
				}
			} catch (DateinValidException e) {
				JOptionPane.showMessageDialog(bookView, "請輸入有效的日期", "錯誤", 2);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(bookView, "日期必須為純數字", "錯誤", 2);
			} catch(DateExcessException e3) {
				JOptionPane.showMessageDialog(bookView, "已過的日期不可訂房", "錯誤", 2);
			}
		}

	}
	//BookView Ends
	
	
	//SearchView (CheckOrder) Begins
	public class SearchToCheckListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				checkChangeModel.searchOrder(searchViewForCheck.getSerialNumber());
				HotelOrder hotelOrder = checkChangeModel.getOrder();
				if (searchViewForCheck.getSerialNumber() < 0)
					throw new NumberFormatException();
				if (hotelOrder == null)
					throw new NoMatchOrderException();
				//JOptionPane.showMessageDialog(searchViewForCheck, checkChangeModel.getOrder().showOrder(), "訂單確認", 1);
				checkView.set(hotelOrder);
				checkView.setVisible(true);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(searchViewForChange, "請輸入9位的正整數", "錯誤", 2);
			} catch (NoMatchOrderException e2) {
				JOptionPane.showMessageDialog(searchViewForChange, "查無此訂位代號", "錯誤", 2);
			}

		}

	}
	//SearchView (CheckOrder) Ends

	
	//SearchView (ChangeOrder) Begins
	public class SearchToChangeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				checkChangeModel.searchOrder(searchViewForChange.getSerialNumber());
				HotelOrder hotelOrder = checkChangeModel.getOrder();
				if (searchViewForChange.getSerialNumber() < 0)
					throw new NumberFormatException();
				if (hotelOrder == null)
					throw new NoMatchOrderException();
				if(hotelOrder.getDayIn()<=ZonedDateTime.now().getDayOfMonth())
					throw new DateExcessException();
				int yesOrNo = JOptionPane.showConfirmDialog(searchViewForChange,
						hotelOrder.showOrder() + "   <br/>確定修改此訂單嗎?", "訂單修改", JOptionPane.YES_NO_OPTION);
				if (yesOrNo == 0) {
					searchViewForChange.setSerialNumber("");
					changeView.setSingleRoom(hotelOrder.getSingleRoomRequired());
					changeView.setDoubleRoom(hotelOrder.getDoubleRoomRequired());
					changeView.setQuadRoom(hotelOrder.getQuadRoomRequired());
					changeView.setDayIn(hotelOrder.getDayIn());
					changeView.setDayOut(hotelOrder.getDayOut());
					searchViewForChange.setVisible(false);
					changeView.setVisible(true);
				}
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(searchViewForChange, "請輸入9位的正整數", "錯誤", 2);
			} catch (NoMatchOrderException e2) {
				JOptionPane.showMessageDialog(searchViewForChange, "查無此訂位代號", "錯誤", 2);
			}catch(DateExcessException e3) {
				JOptionPane.showMessageDialog(searchViewForChange, "已超過修改及退訂時間", "錯誤", 2);
			}
		}
	}
	//SearchView (ChangeOrder) Ends

	//ChangeView begins
	public class ChangeDateListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (changeView.getDayIn() < 0 || changeView.getDayIn() > 30 || changeView.getDayOut() < 0
						|| changeView.getDayOut() > 30 || changeView.getDayIn() >= changeView.getDayOut())
					throw new DateinValidException();
				if(changeView.getDayIn()<ZonedDateTime.now().getDayOfMonth())
					throw new DateExcessException();
				//HotelOrder order = checkChangeModel.getOrder();
				int yesOrNo = JOptionPane.showConfirmDialog(changeView, "確定修改訂單?", "修改確認", JOptionPane.YES_NO_OPTION);
				if (yesOrNo == 0) {
					checkChangeModel.changeDate(changeView.getDayIn(), changeView.getDayOut());
					homeView.setVisible(true);
					changeView.setVisible(false);
					JOptionPane.showMessageDialog(changeView, "修改成功", "", 1);
				}
			} catch (NotEnoughVacancyException e1) {
				JOptionPane.showMessageDialog(changeView, "空房不足", "錯誤", 2);
			} catch (DateinValidException e2) {
				JOptionPane.showMessageDialog(changeView, "請輸入有效的日期", "錯誤", 2);
			} catch (NumberFormatException e1) {
				JOptionPane.showMessageDialog(changeView, "日期必須為純數字", "錯誤", 2);
			}catch (DateExcessException e3) {
				JOptionPane.showMessageDialog(changeView, "已過的日期不可訂房", "錯誤", 2);
			}

		}
	}

	public class ChangeRoomNumberListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				int yesOrNo = JOptionPane.showConfirmDialog(changeView, "確定修改訂單?", "修改確認", JOptionPane.YES_NO_OPTION);
				if (yesOrNo == 0) {
					checkChangeModel.changeNumber(changeView.getSingleRoom(), changeView.getDoubleRoom(),
							changeView.getQuadRoom());
					homeView.setVisible(true);
					changeView.setVisible(false);
					JOptionPane.showMessageDialog(changeView, "修改成功", "", 1);
				}
			} catch (NotEnoughVacancyException e1) {
				JOptionPane.showMessageDialog(changeView, "空房不足", "錯誤", 2);
			}
		}
	}
	public class DeleteButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int yesOrNo = JOptionPane.showConfirmDialog(changeView, "確定取消訂單?", "退訂確認", JOptionPane.YES_NO_OPTION);
			if (yesOrNo == 0) {
				try {

					checkChangeModel.deleteOrder();
					JOptionPane.showMessageDialog(changeView, "退訂成功", "", 1);
					homeView.setVisible(true);
					changeView.setVisible(false);
				} catch (NotEnoughVacancyException e1) {
					JOptionPane.showMessageDialog(changeView, "空房不足? WTF", "錯誤", 2);
				}
			}
		}

	}
	
	//ChangeView Ends
	
	
	// CheckView Begins
	public class GoHomeListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			homeView.setVisible(true);
			bookView.setVisible(false);
			bookView.clear();
			searchViewForCheck.setVisible(false);
			searchViewForCheck.clear();
			searchViewForChange.setVisible(false);
			searchViewForChange.clear();
			changeView.setVisible(false);
			changeView.clear();
			checkView.setVisible(false);

		}

	}
	// CheckView Ends
	
	public void setUserLable(String userName) {
		homeView.setUserLable(userName);
		checkView.setUserLable(userName);
		bookView.setUserLable(userName);
		searchViewForCheck.setUserLable(userName);
		searchViewForChange.setUserLable(userName);
	}
	
	
}