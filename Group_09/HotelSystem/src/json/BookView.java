package json;
import javax.swing.*;

import java.awt.event.*;
import javax.swing.border.EmptyBorder;

import helpingClasses.HotelOrder;

import java.awt.Color;
import java.awt.Font;

public class BookView extends JFrame {

	private JPanel contentPane;
	private JTextField yearIn;
	private JLabel label;
	private JLabel label_1;
	private JTextField monthIn;
	private JTextField dayIn;
	private JLabel label_2;
	private JLabel label_3;
	private JTextField yearOut;
	private JLabel label_4;
	private JTextField monthOut;
	private JLabel label_5;
	private JLabel label_6;
	private JLabel label_7;
	private JLabel label_8;
	private JLabel label_9;
	private JTextField dayOut;
	private DefaultListModel<HotelOrder> listModel;
	private JLabel label_10;
	private JLabel label_11;
	private JButton searchingButton;
	private JButton bookingButton;
	private JScrollPane scrollPane;
	private JSpinner singleRoomSpinner;
	private JSpinner doubleRoomSpinner;
	private JSpinner quadRoomSpinner;
	private JList<HotelOrder> list;
	private JRadioButton oneStarButton;
	private JRadioButton twoStarButton;
	private JRadioButton threeStarButton;
	private JRadioButton fourStarButton;
	private JRadioButton fiveStarButton;
	private JButton homeButton;
	private ButtonGroup buttonGroup;
	private JLabel lblNewLabel;
	private JLabel userLable;
	private JButton logOutButton;
	private JLabel lblNewLabel_2;
	private JLabel label_12;
	private JLabel lblNewLabel_3;

	/**
	 * Launch the application.
	 */
	/*
	 * public static void main(String[] args) { EventQueue.invokeLater(new
	 * Runnable() { public void run() { try { SearchPage frame = new SearchPage();
	 * frame.setVisible(true); } catch (Exception e) { e.printStackTrace(); } } });
	 * }
	 */

	/**
	 * Create the frame.
	 */
	public BookView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		contentPane = new JPanel();
		setBounds(0, 0, 1920, 1080);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		label = new JLabel("\u5165\u4F4F\u65E5\u671F");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label.setBounds(208, 94, 117, 40);
		contentPane.add(label);

		yearIn = new JTextField("2019");
		yearIn.setFont(new Font("新細明體", Font.PLAIN, 24));
		yearIn.setEnabled(false);
		yearIn.setBounds(208, 134, 116, 40);
		contentPane.add(yearIn);
		yearIn.setColumns(10);

		label_1 = new JLabel("/");
		label_1.setBounds(372, 137, 19, 19);
		contentPane.add(label_1);

		monthIn = new JTextField("6");
		monthIn.setFont(new Font("新細明體", Font.PLAIN, 24));
		monthIn.setEnabled(false);
		monthIn.setBounds(417, 134, 56, 40);
		contentPane.add(monthIn);
		monthIn.setColumns(10);

		dayIn = new JTextField();
		dayIn.setFont(new Font("新細明體", Font.PLAIN, 24));
		dayIn.setBounds(570, 134, 56, 40);
		dayIn.setColumns(10);
		contentPane.add(dayIn);

		label_2 = new JLabel("/");
		label_2.setBounds(512, 137, 19, 19);
		contentPane.add(label_2);

		label_3 = new JLabel("\u9000\u623F\u65E5\u671F");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_3.setBounds(207, 180, 117, 40);
		contentPane.add(label_3);

		yearOut = new JTextField("2019");
		yearOut.setFont(new Font("新細明體", Font.PLAIN, 24));
		yearOut.setEnabled(false);
		yearOut.setBounds(208, 219, 116, 40);
		yearOut.setColumns(10);
		contentPane.add(yearOut);

		label_4 = new JLabel("/");
		label_4.setBounds(372, 222, 19, 19);
		contentPane.add(label_4);

		monthOut = new JTextField("6");
		monthOut.setFont(new Font("新細明體", Font.PLAIN, 24));
		monthOut.setEnabled(false);
		monthOut.setBounds(417, 219, 56, 40);
		monthOut.setColumns(10);
		contentPane.add(monthOut);

		label_5 = new JLabel("/");
		label_5.setBounds(512, 222, 19, 19);
		contentPane.add(label_5);

		dayOut = new JTextField();
		dayOut.setFont(new Font("新細明體", Font.PLAIN, 24));
		dayOut.setBounds(570, 219, 56, 40);
		dayOut.setColumns(10);
		contentPane.add(dayOut);

		searchingButton = new JButton("\u641C\u5C0B");
		searchingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		searchingButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		searchingButton.setBounds(441, 773, 200, 50);
		searchingButton.setBackground(Color.ORANGE);
		searchingButton.setContentAreaFilled(false);
		searchingButton.setOpaque(true);
		contentPane.add(searchingButton);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(1100, 200, 388, 450);
		scrollPane.setHorizontalScrollBar(null);
		contentPane.add(scrollPane);

		listModel = new DefaultListModel<HotelOrder>();

		list = new JList<HotelOrder>();
		list.setLayoutOrientation(JList.VERTICAL);
		list.setModel(listModel);
		scrollPane.setColumnHeaderView(list);
		
		userLable = new JLabel();
		userLable.setFont(new Font("新細明體", Font.PLAIN, 24));
		userLable.setBounds(1623, 0, 127, 40);
		contentPane.add(userLable);

		label_6 = new JLabel("\u55AE\u4EBA\u623F");
		label_6.setForeground(Color.WHITE);
		label_6.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_6.setBounds(208, 344, 96, 33);
		contentPane.add(label_6);

		label_7 = new JLabel("\u96D9\u4EBA\u623F");
		label_7.setForeground(Color.WHITE);
		label_7.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_7.setBounds(392, 351, 81, 33);
		contentPane.add(label_7);

		label_8 = new JLabel("\u56DB\u4EBA\u623F");
		label_8.setForeground(Color.WHITE);
		label_8.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_8.setBounds(570, 351, 96, 33);
		contentPane.add(label_8);

		singleRoomSpinner = new JSpinner();
		singleRoomSpinner.setFont(new Font("新細明體", Font.PLAIN, 24));
		singleRoomSpinner.setBounds(208, 421, 34, 26);
		singleRoomSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		contentPane.add(singleRoomSpinner);

		doubleRoomSpinner = new JSpinner();
		doubleRoomSpinner.setFont(new Font("新細明體", Font.PLAIN, 24));
		doubleRoomSpinner.setBounds(392, 421, 34, 26);
		doubleRoomSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		contentPane.add(doubleRoomSpinner);

		quadRoomSpinner = new JSpinner();
		quadRoomSpinner.setFont(new Font("新細明體", Font.PLAIN, 24));
		quadRoomSpinner.setBounds(570, 421, 34, 26);
		quadRoomSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		contentPane.add(quadRoomSpinner);

		label_9 = new JLabel("\u9593");
		label_9.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_9.setForeground(Color.WHITE);
		label_9.setBounds(241, 415, 404, 33);
		contentPane.add(label_9);

		label_10 = new JLabel("\u9593");
		label_10.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_10.setForeground(Color.WHITE);
		label_10.setBounds(428, 415, 235, 33);
		contentPane.add(label_10);

		label_11 = new JLabel("\u9593");
		label_11.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_11.setForeground(Color.WHITE);
		label_11.setBounds(608, 415, 58, 33);
		contentPane.add(label_11);

		oneStarButton = new JRadioButton("\u4E00\u661F");
		oneStarButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		oneStarButton.setForeground(Color.BLACK);
		oneStarButton.setBounds(197, 671, 127, 50);
		contentPane.add(oneStarButton);

		twoStarButton = new JRadioButton("\u4E8C\u661F");
		twoStarButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		twoStarButton.setForeground(Color.BLACK);
		twoStarButton.setBounds(347, 671, 127, 50);
		contentPane.add(twoStarButton);

		threeStarButton = new JRadioButton("\u4E09\u661F");
		threeStarButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		threeStarButton.setForeground(Color.BLACK);
		threeStarButton.setBounds(499, 671, 127, 50);
		contentPane.add(threeStarButton);

		fourStarButton = new JRadioButton("\u56DB\u661F");
		fourStarButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		fourStarButton.setForeground(Color.BLACK);
		fourStarButton.setBounds(648, 671, 127, 50);
		contentPane.add(fourStarButton);

		fiveStarButton = new JRadioButton("\u4E94\u661F");
		fiveStarButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		fiveStarButton.setForeground(Color.BLACK);
		fiveStarButton.setBounds(793, 671, 127, 50);
		contentPane.add(fiveStarButton);

		buttonGroup = new ButtonGroup();
		buttonGroup.add(oneStarButton);
		buttonGroup.add(twoStarButton);
		buttonGroup.add(threeStarButton);
		buttonGroup.add(fourStarButton);
		buttonGroup.add(fiveStarButton);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		lblNewLabel_1.setBounds(175, 0, 832, 1016);
		lblNewLabel_1.setBackground(new Color(0, 0, 0, 180));
		lblNewLabel_1.setOpaque(true);
		contentPane.add(lblNewLabel_1);

		bookingButton = new JButton("\u8A02\u623F");
		bookingButton.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		bookingButton.setBounds(1168, 773, 235, 50);
		contentPane.add(bookingButton);
		
		homeButton = new JButton("\u56DE\u4E3B\u9801");
		homeButton.setBounds(0, 0, 175, 40);
		contentPane.add(homeButton);
		
		logOutButton = new JButton("\u767B\u51FA");
		logOutButton.setBounds(1745, 0, 175, 40);
		contentPane.add(logOutButton);
		
		lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setBounds(208, 605, 57, 19);
		contentPane.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(1010, 0, 599, 1016);
		lblNewLabel_3.setBackground(Color.YELLOW);
		lblNewLabel_3.setOpaque(true);
		contentPane.add(lblNewLabel_3);
		
		label_12 = new JLabel("\u661F\u7D1A\u9078\u9805");
		label_12.setFont(new Font("微軟正黑體", Font.PLAIN, 24));
		label_12.setForeground(Color.WHITE);
		label_12.setBounds(197, 605, 211, 40);
		contentPane.add(label_12);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\thumb-1920-241216.jpg"));
		lblNewLabel.setBounds(0, 0, 1920, 1080);
		contentPane.add(lblNewLabel);

	}

	public int getSingleRoom() {
		return (Integer) singleRoomSpinner.getValue();
	}

	public int getDoubleRoom() {
		return (Integer) doubleRoomSpinner.getValue();
	}

	public int getQuadRoom() {
		return (Integer) quadRoomSpinner.getValue();
	}

	public int getYearIn() {
		return Integer.parseInt(yearIn.getText());
	}

	public int getMonthIn() {
		return Integer.parseInt(monthIn.getText());
	}

	public int getDayIn() {
		return Integer.parseInt(dayIn.getText());
	}

	public int getYearOut() {
		return Integer.parseInt(yearOut.getText());
	}

	public int getMonthOut() {
		return Integer.parseInt(monthOut.getText());
	}

	public int getDayOut() {
		return Integer.parseInt(dayOut.getText());
	}

	public int getStar() {
		if (oneStarButton.isSelected())
			return 1;
		else if (twoStarButton.isSelected())
			return 2;
		else if (threeStarButton.isSelected())
			return 3;
		else if (fourStarButton.isSelected())
			return 4;
		else if (fiveStarButton.isSelected())
			return 5;
		else
			return 0;
	}

	public void addSearchingListener(ActionListener listener) {
		searchingButton.addActionListener(listener);
	}

	public void clearList() {
		listModel.removeAllElements();
	}
	
	public void clearSelect() {
		list.clearSelection();
	}

	public HotelOrder getSelectedHotel() {
		return (HotelOrder) list.getSelectedValue();
	}

	public void addBookingButtonListener(ActionListener listener) {
		bookingButton.addActionListener(listener);
	}
	public void addHomeButtonListener(ActionListener listener){
		homeButton.addActionListener(listener);
	}

	public void addHotel(HotelOrder hotelInfo) {
		listModel.addElement(hotelInfo);
	}

	public void clear() {
		dayIn.setText("");
		dayOut.setText("");
		buttonGroup.clearSelection();
		singleRoomSpinner.setValue(0);
		doubleRoomSpinner.setValue(0);
		quadRoomSpinner.setValue(0);
		listModel.removeAllElements();
	}
	
	public void setUserLable(String userName) {
		userLable.setText("使用者 : "+userName);
	}
	public void addLogOutListener(ActionListener listener) {
		logOutButton.addActionListener(listener);
	}
}
