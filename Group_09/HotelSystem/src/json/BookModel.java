package json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import exceptions.NotEnoughVacancyException;
import exceptions.NumberGenerateErrorException;
import helpingClasses.HotelOrder;
import helpingClasses.HotelRoomTable;

public class BookModel {

	private HotelRoomTable hotelRoomTable;
	private ArrayList<HotelOrder> orderList;

	public BookModel(HotelRoomTable hotelRoomTable, ArrayList<HotelOrder> orderList)
			throws ClassNotFoundException, IOException {
		this.hotelRoomTable = hotelRoomTable;
		this.orderList = orderList;
	}

	public ArrayList<HotelOrder> search(int star, int singleRoomRequired, int doubleRoomRequired, int quadRoomRequired,
			int dayIn, int dayOut) {
		ArrayList<HotelOrder> searchResult = new ArrayList<HotelOrder>();
		for (int hotelID = 0; hotelID <= 1499; hotelID++) {
			if (hotelRoomTable.roomFile.getHotelStar(hotelID) == star || star == 0) {
				boolean flag = true;
				for (int day = dayIn; day <= dayOut; day++) {
					if (hotelRoomTable.getRoomNumber(hotelID, 1, day) < singleRoomRequired
							|| hotelRoomTable.getRoomNumber(hotelID, 2, day) < doubleRoomRequired
							|| hotelRoomTable.getRoomNumber(hotelID, 3, day) < quadRoomRequired) {
						flag = false;
						break;
					}
				}
				if (flag == true)
					searchResult.add(new HotelOrder(hotelID, hotelRoomTable.roomFile.getHotelStar(hotelID),
							hotelRoomTable.roomFile.getHotelLocation(hotelID),
							hotelRoomTable.roomFile.getHotelAddress(hotelID), singleRoomRequired, doubleRoomRequired,
							quadRoomRequired, hotelRoomTable.roomFile.getRoomPrice(hotelID, 1),
							hotelRoomTable.roomFile.getRoomPrice(hotelID, 2),
							hotelRoomTable.roomFile.getRoomPrice(hotelID, 3),
							hotelRoomTable.roomFile.getRoomPrice(hotelID, 1) * singleRoomRequired
									+ hotelRoomTable.roomFile.getRoomPrice(hotelID, 2) * doubleRoomRequired
									+ hotelRoomTable.roomFile.getRoomPrice(hotelID, 3) * quadRoomRequired,
							dayIn, dayOut));
			}

		}
		Collections.sort(searchResult);
		return searchResult;
		// for (int index = 0; index < searchResult.size(); index++) {
		// System.out.println(searchResult.get(index).getID() + " " +
		// searchResult.get(index).getTotalPrice());
		// }
	}

	private int generateNumber() throws NumberGenerateErrorException {
		
		for (int retry=0;retry<10 ;retry++) {
			Random r = new Random();
			System.out.println("test0");
			int number = r.nextInt(1000000000);
			//System.out.println("test1");
			boolean flag = true;
			for (int index = 0; index < orderList.size(); index++) {
				if (number == Integer.parseInt(orderList.get(index).getSerialNumber())) {
					flag = false;
					break;
				}

			}
			if (flag == true) {
				//System.out.println("test2");
				return number;
			}
		}
		//System.out.println("test3");
		throw new NumberGenerateErrorException();
	}

	public void addOrder(HotelOrder hotelSelected) throws NumberGenerateErrorException, NotEnoughVacancyException {
		for (int day = hotelSelected.getDayIn(); day < hotelSelected.getDayOut(); day++) {
			hotelRoomTable.changeRoomNumber(hotelSelected.getID(), 1, day, -hotelSelected.getSingleRoomRequired());
			hotelRoomTable.changeRoomNumber(hotelSelected.getID(), 2, day, -hotelSelected.getDoubleRoomRequired());
			hotelRoomTable.changeRoomNumber(hotelSelected.getID(), 3, day, -hotelSelected.getQuadRoomRequired());
		}
		hotelSelected.setSerialNumber(generateNumber());
		orderList.add(hotelSelected);
	}

}