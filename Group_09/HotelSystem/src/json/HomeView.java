package json;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class HomeView extends JFrame {

	private JPanel contentPane;
	private JButton goBookButton;
	private JButton goChangeButton;
	private JButton goCheckButton;
	private JLabel userLable;
	private String userName;
	private JButton logOutButton;
	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HomeView frame = new HomeView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	*/
	/**
	 * Create the frame.
	 */
	public HomeView() {
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setBounds(0, 0, 1920, 1080);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		contentPane.setLayout(null);
		
		goBookButton = new JButton("\u627E\u98EF\u5E97 \u8A02\u98EF\u5E97");
		goBookButton.setForeground(Color.WHITE);
		goBookButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		userLable = new JLabel();
		userLable.setForeground(Color.WHITE);
		userLable.setFont(new Font("新細明體", Font.PLAIN, 24));
		userLable.setBounds(1548, 0, 212, 61);
		contentPane.add(userLable);
		goBookButton.setFont(new Font("微軟正黑體", Font.PLAIN, 48));
		goBookButton.setBounds(600, 242, 800, 200);
		goBookButton.setBackground(new Color(0,0,0,240));
		goBookButton.setContentAreaFilled(false);
		goBookButton.setOpaque(true);
		contentPane.add(goBookButton);
		
		goChangeButton = new JButton("\u4FEE\u6539\u8207\u9000\u8A02");
		goChangeButton.setForeground(Color.WHITE);
		goChangeButton.setFont(new Font("微軟正黑體", Font.PLAIN, 48));
		goChangeButton.setBackground(new Color(0,0,0,240));
		goChangeButton.setContentAreaFilled(false);
		goChangeButton.setOpaque(true);
		goChangeButton.setBounds(600, 724, 800, 200);
		
		contentPane.add(goChangeButton);
		
		goCheckButton = new JButton("\u78BA\u8A8D\u8A02\u55AE");
		goCheckButton.setForeground(Color.WHITE);
		goCheckButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		goCheckButton.setFont(new Font("微軟正黑體", Font.PLAIN, 48));
		goCheckButton.setBackground(new Color(0,0,0,240));
		goCheckButton.setContentAreaFilled(false);
		goCheckButton.setOpaque(true);
		goCheckButton.setBounds(600, 480, 800, 200);
		
		contentPane.add(goCheckButton);
		
		JLabel block = new JLabel("");
		block.setBounds(600, 0, 800, 1003);
		block.setBackground(new Color(0, 0, 0, 180));
		block.setOpaque(true); 
		contentPane.add(block);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\table23_1920x1440.jpg"));
		lblNewLabel.setBounds(0, 0, 1920, 1080);
		contentPane.add(lblNewLabel);
		
		logOutButton = new JButton("\u767B\u51FA");
		logOutButton.setBounds(1763, 0, 157, 61);
		contentPane.add(logOutButton);
	}
	public void addGoBookButtonListener(ActionListener listener) {
		goBookButton.addActionListener(listener);
	}
	public void addGoChangeButtonListener(ActionListener listener) {
		goChangeButton.addActionListener(listener);
	}
	
	public void addGoCheckButtonListener(ActionListener listener) {
		goCheckButton.addActionListener(listener);
	}
	public void setUserLable(String userName) {
		this.userName=new String(userName);
		userLable.setText("使用者 : "+userName);
	}
	public String getUserName() {
		return new String(userName);
	}
	
	public void addLogOutListener(ActionListener listener) {
		logOutButton.addActionListener(listener);
	}
	
}
