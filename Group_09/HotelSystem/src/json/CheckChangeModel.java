package json;

import java.util.ArrayList;

import exceptions.NotEnoughVacancyException;
import helpingClasses.HotelOrder;
import helpingClasses.HotelRoomTable;

public class CheckChangeModel {
	private HotelRoomTable hotelRoomTable;
	private ArrayList<HotelOrder> hotelOrderList;
	private int position;
	private HotelOrder order;
	
	public CheckChangeModel(HotelRoomTable hotelRoomTable, ArrayList<HotelOrder> hotelOrderList){
		this.hotelOrderList=hotelOrderList;
		this.hotelRoomTable=hotelRoomTable;
	}
	
	public void searchOrder(int number) {
		for(int index=0;index<hotelOrderList.size();index++) {
			if(Integer.parseInt(hotelOrderList.get(index).getSerialNumber())==number) {
				order=hotelOrderList.get(index);
				position=index;
				}
		}
	}
	public HotelOrder getOrder() {
		return order;
	}
	
	
	public void changeDate(int dayIn, int dayOut) throws NotEnoughVacancyException {
		if(true) {
		for(int day = order.getDayIn();day<dayIn;day++){
			hotelRoomTable.changeRoomNumber(order.getID(), 1, day, order.getSingleRoomRequired());
			hotelRoomTable.changeRoomNumber(order.getID(), 2, day, order.getDoubleRoomRequired());
			hotelRoomTable.changeRoomNumber(order.getID(), 3, day, order.getSingleRoomRequired());
			}
		for(int day = dayOut;day<order.getDayOut();day++){
			hotelRoomTable.changeRoomNumber(order.getID(), 1, day, order.getSingleRoomRequired());
			hotelRoomTable.changeRoomNumber(order.getID(), 2, day, order.getDoubleRoomRequired());
			hotelRoomTable.changeRoomNumber(order.getID(), 3, day, order.getQuadRoomRequired());
		}
		order.setDate(dayIn,dayOut);
		}
		
	}
	public void changeNumber(int singleRoomNumber,int doubleRoomNumber, int quadRoomNumber) throws NotEnoughVacancyException {
		for(int day = order.getDayIn() ;day < order.getDayOut() ; day++) {
			hotelRoomTable.changeRoomNumber(order.getID(), 1, day, order.getSingleRoomRequired()-singleRoomNumber);
			hotelRoomTable.changeRoomNumber(order.getID(), 2, day, order.getDoubleRoomRequired()-doubleRoomNumber);
			hotelRoomTable.changeRoomNumber(order.getID(), 3, day, order.getQuadRoomRequired()-quadRoomNumber);
		}
		order.setRoomNumber(singleRoomNumber, doubleRoomNumber,quadRoomNumber);
	}
	public void deleteOrder() throws NotEnoughVacancyException {
		for(int day = order.getDayIn() ;day < order.getDayOut() ; day++) {
			hotelRoomTable.changeRoomNumber(order.getID(), 1, day, order.getSingleRoomRequired());
			hotelRoomTable.changeRoomNumber(order.getID(), 2, day, order.getDoubleRoomRequired());
			hotelRoomTable.changeRoomNumber(order.getID(), 3, day, order.getQuadRoomRequired());
		}
		hotelOrderList.remove(position);
		order=null;
		position=0;
	}
	
	
}