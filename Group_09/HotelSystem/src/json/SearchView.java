package json;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.ImageIcon;

public class SearchView extends JFrame {
	

	private JPanel contentPane;
	private JTextField serialNumber;
	private JLabel label;
	private JButton searchButton;
	private JButton homeButton;
	private JLabel userLable;
	private JButton logOutButton;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchView frame = new SearchView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/
	
	public SearchView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		contentPane = new JPanel();
		setBounds(0, 0, 1920, 1080);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		serialNumber = new JTextField();
		serialNumber.setFont(new Font("新細明體", Font.PLAIN, 24));
		serialNumber.setBounds(858, 506, 237, 52);
		contentPane.add(serialNumber);
		serialNumber.setColumns(10);
		
		label = new JLabel("\u8A02\u55AE\u7DE8\u865F:");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("新細明體", Font.PLAIN, 24));
		label.setBounds(921, 420, 143, 44);
		contentPane.add(label);
		
		searchButton = new JButton("\u67E5\u8A62");
		searchButton.setBounds(921, 675, 99, 27);
		contentPane.add(searchButton);
		
		homeButton = new JButton("\u56DE\u4E3B\u9801");
		homeButton.setBounds(0, 0, 150, 68);
		contentPane.add(homeButton);
		
		userLable = new JLabel();
		userLable.setFont(new Font("新細明體", Font.PLAIN, 24));
		userLable.setBounds(1579, 0, 182, 68);
		contentPane.add(userLable);
		
		logOutButton = new JButton("\u767B\u51FA");
		logOutButton.setBounds(1758, 0, 162, 68);
		contentPane.add(logOutButton);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(676, 0, 642, 1020);
		lblNewLabel_1.setBackground(new Color(0, 0, 0, 180));
		lblNewLabel_1.setOpaque(true);
		contentPane.add(lblNewLabel_1);
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Owner\\eclipse-workspace\\HotelSystem\\wallpaper\\thumb-1920-591442.jpg"));
		lblNewLabel.setBounds(0, 0, 1920, 1020);
		contentPane.add(lblNewLabel);
	}
	
	
	
	public int getSerialNumber() {
		return Integer.parseInt(serialNumber.getText());
	}
	public void setSerialNumber(String number) {
		serialNumber.setText(number);
	}
	
	public void addButtonListener(ActionListener listener) {
		searchButton.addActionListener(listener);
	}

	public void addHomeButtonListener(ActionListener listener) {
		homeButton.addActionListener(listener);
		
	}
	
	public void clear() {
		serialNumber.setText("");
	}
	
	public void setUserLable(String userName) {
		userLable.setText("使用者 : "+userName);
	}
	
	public void addLogOutListener(ActionListener listener) {
		logOutButton.addActionListener(listener);
	}
}
