package exceptions;

public class NotEnoughVacancyException extends Exception {
	public NotEnoughVacancyException() {
		super("空房數量不足");
	}
}
