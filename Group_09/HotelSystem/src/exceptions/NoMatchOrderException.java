package exceptions;

public class NoMatchOrderException extends Exception {
	public NoMatchOrderException() {
		super("查無此訂位代號");
	}
}
