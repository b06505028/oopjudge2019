package helpingClasses;

public class HotelOrder implements Comparable<HotelOrder> {
	private int hotelID;
	private int hotelStar;
	private String hotelLocation;
	private String hotelAddress;
	private int singleRoomRequired;
	private int doubleRoomRequired;
	private int quadRoomRequired;
	private int singleRoomPrice;
	private int doubleRoomPrice;
	private int quadRoomPrice;
	private int totalPrice;
	private int dayIn;
	private int dayOut;
	
	private String serialNumber;

	public HotelOrder() {
		hotelLocation = " ";
		hotelAddress = " ";
		serialNumber=null;
	}

	public HotelOrder(int hotelID, int hotelStar, String hotelLocation, String hotelAddress, int singleRoomRequired, int doubleRoomRequired, int quadRoomRequired, int singleRoomPrice,
			int doubleRoomPrice, int quadRoomPrice, int totalPrice,int dayIn,int dayOut) {
		
		this.hotelID = hotelID;
		this.hotelStar = hotelStar;
		this.hotelLocation = hotelLocation;
		this.hotelAddress = hotelAddress;
		this.singleRoomRequired=singleRoomRequired;
		this.doubleRoomRequired=doubleRoomRequired;
		this.quadRoomRequired=quadRoomRequired;
		this.singleRoomPrice = singleRoomPrice;
		this.doubleRoomPrice = doubleRoomPrice;
		this.quadRoomPrice = quadRoomPrice;
		this.totalPrice = totalPrice;
		this.dayIn=dayIn;
		this.dayOut=dayOut;


	}

	@Override 
	public int compareTo(HotelOrder other) {
		HotelOrder temp = (HotelOrder) other;
		if (totalPrice > temp.totalPrice)
			return 1;
		else if (totalPrice == temp.totalPrice)
			return 0;
		else
			return -1;
	}

	@Override 
	public String toString() {
		StringBuilder temp=new StringBuilder();
		temp.append("<html>飯店ID : ").append(hotelID).append("<br/>星級 : ").append(hotelStar).append("<br/>地點 : ").append(hotelLocation).append("<br/>地址 : ").append(hotelAddress).append("<br/>單人房 : ").append(singleRoomPrice).append(" / 每間<br/>雙人房 : ").append(doubleRoomPrice).append(" / 每間<br/>四人房 : ").append(quadRoomPrice).append(" / 每間<br/>總價 : ").append(totalPrice).append("<br/>  <html>");
		return new String(temp);
		/*return "<html>HotelID: " + hotelID + "<br/>HotelStar: " + hotelStar + "<br/>Locality: " + hotelLocation
		+ "<br/>Street-Address: " + hotelAddress + "<br/>Single room : " + singleRoomPrice
		+ " per room<br/>Double room : " + doubleRoomPrice + " per room<br/>Quad room : " + quadRoomPrice
		+ " per room<br/>Total price :" + totalPrice + "<br/>  <html>";*/
	}
	
	public String showOrder() {
		StringBuilder temp=new StringBuilder();
		temp.append("<html>飯店ID : ").append(hotelID).append("<br/>星級 : ").append(hotelStar).append("<br/>地點 : ").append(hotelLocation).append("<br/>地址 : ").append(hotelAddress).append("<br/>單人房 : ").append(singleRoomRequired).append(" 間<br/>雙人房 : ").append(doubleRoomRequired).append(" 間<br/>四人房 : ").append(quadRoomRequired).append(" 間<br/>入住日期 : 2019 / 6 / ").append(dayIn).append("<br/>退房日期 : 2019 / 6 / ").append(dayOut).append("<br/>訂單編號 : ").append(serialNumber);
		return new String(temp);
	}
	
	
	public int getID() {
		return hotelID;
	}

	public int getHotelStar() {
		return hotelStar;
	}

	public String getLocation() {
		return new String(hotelLocation);
	}

	public String getAddress() {
		return new String(hotelAddress);
	}
	
	public int getSingleRoomRequired() {
		return singleRoomRequired;
	}

	public void setSingleRoomRequired(int singleRoomRequired) {
		this.singleRoomRequired = singleRoomRequired;
	}
	
	public int getDoubleRoomRequired() {
		return doubleRoomRequired;
	}

	public void setDoubleRoomRequired(int doubleRoomRequired) {
		this.doubleRoomRequired = doubleRoomRequired;
	}
	

	public int getQuadRoomRequired() {
		return quadRoomRequired;
	}

	public void setQuadRoomRequired(int quadRoomRequired) {
		this.quadRoomRequired = quadRoomRequired;
	}

	public int getSingleRoomPrice() {
		return singleRoomPrice;
	}

	public int getDoubleRoomPrice() {
		return doubleRoomPrice;
	}

	public int getQuadRoomPrice() {
		return quadRoomPrice;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setDayIn(int dayIn) {
		this.dayIn = dayIn;
	}
	public int getDayIn() {
		return dayIn;
	}
	
	public void setDayOut(int dayOut) {
		this.dayOut = dayOut;
	}
	public int getDayOut() {
		return dayOut;
	}
	
	public String getSerialNumber() {
		return new String(serialNumber);
	}

	public void setSerialNumber(int number) {
		serialNumber=String.format("%09d", number);
	}
	
	public void setRoomNumber(int singleRoomNumber,int doubleRoomNumber,int quadRoomNumber) {
		this.singleRoomRequired = singleRoomNumber;
		this.doubleRoomRequired = doubleRoomNumber;
		this.quadRoomRequired = quadRoomNumber;
	}

	public void setDate(int dayIn,int dayOut) {
		this.dayIn=dayIn;
		this.dayOut=dayOut;
		
	}
}