package helpingClasses;

import java.io.IOException;

import exceptions.NotEnoughVacancyException;

public class HotelRoomTable {
	private int[][] hotelRoomArray;
	public ReadJSONFile roomFile;

	public HotelRoomTable() throws ClassNotFoundException, IOException {
		roomFile = new ReadJSONFile(
				"C:\\Users\\Owner\\eclipse-workspace\\HotelSearch-model\\src\\HotelList.json");
		
		hotelRoomArray = new int[4500][30];

		for (int HotelID = 0; HotelID < 1500; HotelID++) {
			for (int HotelRoomType = 1; HotelRoomType <= 3; HotelRoomType++) {
				for (int Date = 1; Date <= 30; Date++) {
					int i = HotelID * 3 + (HotelRoomType - 1);
					int j = Date - 1;
					hotelRoomArray[i][j] = roomFile.getRoomNumber(HotelID, HotelRoomType);
				}
			}
		}
	}
	public int getRoomNumber(int hotelID,int roomType,int Day) {
		return hotelRoomArray[hotelID*3+roomType - 1][Day-1];
	}

	public void changeRoomNumber(int hotelID,int roomType,int Day,int numberChanged) throws NotEnoughVacancyException {
		if(hotelRoomArray[hotelID*3+roomType - 1][Day]+numberChanged<0)
			throw new NotEnoughVacancyException();
		hotelRoomArray[hotelID*3+roomType - 1][Day]+=numberChanged;
	}
}
