package helpingClasses;

import java.io.*;
import org.json.*;

public class ReadJSONFile {
	int roomType;

	private JSONArray HotelList;

	public ReadJSONFile() {

	}

	public ReadJSONFile(String FilePath) throws IOException, ClassNotFoundException {
		System.out.println("json reader launching...");
		FileInputStream FIO = new FileInputStream(FilePath);
		String temp = "";

//		ObjectInputStream obj = new ObjectInputStream (new FileInputStream(""));
//		String Hotel = (String) obj.readObject(); 
//		HotelList = new JSONArray(Hotel);
//		
		BufferedReader buf = new BufferedReader(new InputStreamReader(FIO, "BIG5"));
		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();

		while (line != null) {
			sb.append(line).append("\n");
			line = buf.readLine();
		}
		temp = sb.toString();
		HotelList = new JSONArray(temp);
	}

	public JSONArray getHotelList() {
		return HotelList;

	}

	public JSONObject getHotelData() {
		return null;
	}

	public int getHotelStar(int ID) {
		return HotelList.getJSONObject(ID).getInt("HotelStar");
	}

	public String getHotelLocation(int ID) {
		return HotelList.getJSONObject(ID).getString("Locality");
	}

	public String getHotelAddress(int ID) {
		return HotelList.getJSONObject(ID).getString("Street-Address");
	}

	public JSONArray getHotelRoom(int ID) {
		return HotelList.getJSONObject(ID).getJSONArray("Rooms");
	}

	public int getRoomPrice(int ID, int RoomType) {
		int price = 0;
		JSONArray rooms = getHotelRoom(ID);
		switch (RoomType) {
		case 1:
			price = rooms.getJSONObject(0).getInt("RoomPrice");
			break;
		case 2:
			price = rooms.getJSONObject(1).getInt("RoomPrice");
			break;
		case 3:
			price = rooms.getJSONObject(2).getInt("RoomPrice");
			break;

		}
		return price;

	}

	public int getRoomNumber(int ID, int RoomType) {
		int price = 0;
		JSONArray rooms = getHotelRoom(ID);
		switch (RoomType) {
		case 1:
			price = rooms.getJSONObject(0).getInt("Number");
			break;
		case 2:
			price = rooms.getJSONObject(1).getInt("Number");
			break;
		case 3:
			price = rooms.getJSONObject(2).getInt("Number");
			break;

		}
		return price;

	}

}

/**
 * @author CHU
 *
 *         JSONArray getHotelList(); JSONObject getHotelData(); int
 *         getHotelID(); int getHotelStar(); String getHotelLocation(); String
 *         getHotelAddress(); JSONArray getHotelRoom(); int getRoomPrice(); int
 *         getRoomNumber();
 *
 */